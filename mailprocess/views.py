from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
import json
from .models import ContactUs, Country



def home(request):
    template_name = 'home.html'
    discount_6_mon = settings.DURATION_DISCOUNT.get("2")
    discount_12_mon = settings.DURATION_DISCOUNT.get("3")
    return render(request, template_name, {'country_list': Country.objects.all(), 'discount_6_mon': discount_6_mon,
                                           'discount_12_mon': discount_12_mon})




def contactus(request):
    if request.method == "POST":
        try:
            contact = ContactUs()
            contact.name = request.POST.get('name')
            contact.email = request.POST.get('email')
            contact.message = request.POST.get('message')
            contact.page = int(request.POST.get('page'))
            contact.skypeid = request.POST.get('skypeid')
            contact.mobile = int(request.POST.get('whatsapp_number'))
            contact.country_id = int(request.POST.get('country'))
            contact.status = 1
            contact.save()
            return HttpResponse(json.dumps(1), content_type="application/json")
        except:
            return HttpResponse(json.dumps(2), content_type="application/json")



