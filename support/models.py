from django.db import models
from django.contrib.auth.models import User
# from sites.models import Site
from product.models import Product

STATUS_CHOICE = ((1, 'OPEN'), (2, 'IN PROCESS'), (3, 'RESOLVED'), (5, 'REOPEN'), (4, 'CLOSED'))
SEVERITY_CHOICE = ((1, 'LOW'), (2, 'MEDIUM'), (3, 'HIGH'))


class SupportPlanCategory(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    active = models.BooleanField(default=True)
    modified_date = models.DateTimeField(auto_now=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)


class SupportCases(models.Model):
    subject = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    severity = models.IntegerField(choices=SEVERITY_CHOICE, default=1)
    status = models.IntegerField(choices=STATUS_CHOICE, default=1)
    supportcategory = models.ForeignKey(SupportPlanCategory, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, blank=True)
    active = models.BooleanField(default=True)
    modified_date = models.DateTimeField(auto_now=True)
    date = models.DateTimeField(auto_now_add=True)
    # site=models.ForeignKey(Site)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pk)


class SupportCasesDetail(models.Model):
    supportcases = models.ForeignKey(SupportCases, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.TextField(null=True, blank=True)
    active = models.BooleanField(default=True)
    modified_date = models.DateTimeField(auto_now=True)
