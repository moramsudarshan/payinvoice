from django.conf.urls import url
from .views import ProductView, CustomerList, ProductDetailView,\
    CustomerDetail, Pdfview, NestedInvoiceView, NestedInvoiceDetailView
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [

    url(r'^products/$', ProductView.as_view(), name='ProductView'),
    url(r'^products/(?P<pk>[\w-]+)/$', ProductDetailView.as_view(), name='ProductDetail'),


    url(r'^customers/$', CustomerList.as_view(), name='CustomerList'),
    url(r'^customers/(?P<pk>\d+)/$', CustomerDetail.as_view(), name='CustomerDetail'),


    # url(r'^invoices/$', InvoiceView.as_view(), name='invoice'),
    # url(r'^invoices/(?P<pk>[\w-]+)/$', InvoiceDetailview.as_view(), name='InvoiceDetail'),
    # url(r'^invoices/(?P<pk>[\w-]+)/$', InvoiceDetailview.as_view(), name='InvoiceUpdate'),
    url(r'^invoices/pdf/(?P<pk>[\w-]+)/$', Pdfview.as_view(), name='Invoice'),

  #  url(r'^invoicess/$', pinvoice.as_view(), name='invoice'),

    url(r'^invoices/$', NestedInvoiceView.as_view(), name="Invoices"),
    url(r'^invoices/(?P<pk>[0-9]+)/$', NestedInvoiceDetailView.as_view(), name="InvoiceDetail"),
]


urlpatterns = format_suffix_patterns(urlpatterns)
