from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
import json
from django.db.models import Q, Sum
import requests
from decimal import Decimal
import datetime
from payment.models import PaymentPlan, PaymentTrack
#from contactlist_manager.utils import subscribers_count
from coupons.models import Coupon, CouponUsage
from django.conf import settings


@login_required
def payment_history(request):
    payment_history = PaymentTrack.objects.filter(status='2', user=request.user).order_by("-payment_date_time")
    total = payment_history.aggregate(Sum('amount'))
    template_name = 'myadmin/payment/payment_history.html'
    return render(request, template_name, {'payment_history': payment_history, 'total': total.get("amount__sum")})


@login_required
def purchase_credit(request, plan_id=None):
    if plan_id:
        invalid = 0
        invalid_msg = ""
        couponcode = None
        value = request.GET.get('value', '1')
        coupon_type = request.GET.get('coupon_type', 'duration')
        discount_in_percent = 0
        if coupon_type == "duration" and value:
            duration = value
            discount_in_percent = settings.DURATION_DISCOUNT.get(duration)
        elif coupon_type == "couponcode" and value:
            duration = "1"
        else:
            duration = "1"
        plan = PaymentPlan.objects.get(id=int(plan_id))
        subtotal_price = total_price = plan.price
        try:
            # res=requests.get("https://finance.google.com/finance/converter?a=1&from=USD&to=INR")
            # res=requests.get("https://www.google.co.in/search?q=usd+to+rs+converter")
            # rate=Decimal(res.text.split("1 United States Dollar = ")[1].split("Indian Rupee")[0].strip())
            res = requests.get("https://api.ratesapi.io/api/latest?base=USD")
            rate = Decimal(res.json().get('rates').get('INR'))
        except:
            rate = Decimal(73.00)
        if coupon_type == "duration" and duration == "2":
            total_price = 6 * total_price
            # subtotal_price = total_price-total_price*discount_in_percent/100
        elif coupon_type == "duration" and duration == "3":
            total_price = 12 * total_price
            # subtotal_price = total_price-total_price*discount_in_percent/100
        elif coupon_type == "couponcode" and value:
            try:
                couponcode = Coupon.objects.get(coupon_code=value, expires_on__gte=datetime.datetime.today())
                if couponcode.user and couponcode.user != request.user:
                    invalid = 2
                    invalid_msg = "CouponCode <b>%s</b> is not applicable for your account. Please contact support team for more details." % (
                        couponcode.coupon_code)
                if couponcode.coupon_type == '2' and invalid == 0:
                    if couponcode.plan_or_category.id != plan.id:
                        invalid = 4
                        invalid_msg = "CouponCode <b>%s</b> is applicable only for <b>%s</b> plan. Please contact support team for more details." % (
                            couponcode.coupon_code, couponcode.plan_or_category.name)
                elif couponcode.coupon_type == '3' and invalid == 0:
                    if couponcode.plan_or_category.plan_type != plan.plan_type:
                        invalid = 5
                        invalid_msg = "CouponCode <b>%s</b> is applicable only for <b>%s</b>. Please contact support team for more details." % (
                            couponcode.coupon_code, couponcode.plan_or_category.get_plan_type_display())
                if couponcode.condition == 2 and invalid == 0:
                    try:
                        payment_obj = PaymentTrack.objects.get(user=request.user, status='2')
                        invalid = 3
                        invalid_msg = "CouponCode <b>%s</b> is applicable on First Payment Only." % (
                            couponcode.coupon_code)
                    except Exception as e:
                        pass
                if invalid == 0:
                    discount_in_percent = couponcode.discount
            except Exception as e:
                invalid = 1
                invalid_msg = "CouponCode <b>%s</b> is not valid." % (value)

        subtotal_price = total_price - total_price * float(discount_in_percent) / 100
        paymenttrack = PaymentTrack(status='1', source="1", duration=duration, user=request.user, amount=subtotal_price,
                                    plan_id=int(plan_id), rate=rate)
        paymenttrack.save()
        if couponcode and invalid == 0 and coupon_type == "couponcode":
            couponusage = CouponUsage(coupon=couponcode, payment=paymenttrack, user=request.user)
            couponusage.save()
        # save coupon usage
        request.session["order_id"] = paymenttrack.id
        template_name = 'myadmin/payment/payment.html'
        return render(request, template_name, {'paymenttotal': Decimal(total_price) * rate, 'total_price': total_price,
                                               'subtotal_price': subtotal_price,
                                               'paymentsubtotal': Decimal(subtotal_price) * rate, 'duration': duration,
                                               'discount_in_percent': discount_in_percent, 'invalid': invalid,
                                               'invalid_msg': invalid_msg})

    #payment_plan_subscriber = None
    #plan_type_id = request.GET.get('plan_type')
    payment_plan = PaymentPlan.objects.filter(active=True)
    #if plan_type_id:
    #    payment_plan = payment_plan.filter(plan_type=plan_type_id)
    # if plan_type_id and int(plan_type_id) == 3:
    #     payment_plan_transactional = payment_plan.exclude(id=request.user.userdetail.apiplan.id)
    # elif plan_type_id and int(plan_type_id) == 7:
    #     payment_plan_sms = payment_plan.exclude(id=request.user.userdetail.smsplan.id)
    #
    # elif plan_type_id and int(plan_type_id) == 6:
    #     payment_plan_push = payment_plan.exclude(id=request.user.userdetail.pushplan.id)
    #else:
    payment_plan_subscriber = payment_plan.filter(plan_type=1)
    payment_plan_subscriber = payment_plan_subscriber.exclude(id=request.user.userdetail.myplan.id)
    template_name = 'myadmin/payment/purchase_credit.html'
    return render(request, template_name,
                  {'payment_plan_subscriber': payment_plan_subscriber,})


def payment(request):
    payment_id = request.GET.get("payment_id")
    status = request.GET.get("status")
    order_id = request.session.get('order_id', False)
    paypal = request.GET.get("paypal")
    if status == 'success' and payment_id and order_id:
        if paypal:
            amount = 0
        else:
            headers = {'X-Api-Key': '042d039b340b833c986a402810732d9d',
                       'X-Auth-Token': 'fe47501667baf724c292e4f84c877759'}

            res = requests.get('https://www.instamojo.com/api/1.1/payments/%s/' % (payment_id), headers=headers)
            value = json.loads(res.text)
            amount = 0  # int(float(value.get('payment').get('amount')))

        paymenttrack = PaymentTrack.objects.get(id=order_id)
        paymenttrack.status = "2"
        paymenttrack.payment_id = payment_id
        paymenttrack.payment_date_time = datetime.datetime.now()
        if amount == 0:
            paymenttrack.amount_received = paymenttrack.amount
        else:
            paymenttrack.amount_received = amount
        paymenttrack.save()
        duration = paymenttrack.duration
        counter = 1
        if duration == '2':
            counter = 6
        elif duration == '3':
            counter = 12
        else:
            counter = 1
        if paymenttrack.plan.plan_type == 4:
             profile = request.user.userdetail
             profile.supportplan = paymenttrack.plan
             profile.supportplan_valid_date = datetime.datetime.now() + datetime.timedelta(days=counter * 30)
             profile.save()

        if paymenttrack.plan.plan_type == 1:
            profile = request.user.userdetail
            profile.myplan = paymenttrack.plan
            profile.total_subscribers = paymenttrack.plan.count
            profile.remaining_subscribers = paymenttrack.plan.count #- subscribers_count(request.user)
            profile.valid_date = datetime.datetime.now() + datetime.timedelta(days=counter * 30)
            profile.save()

        try:
            del request.session['order_id']
        except Exception as e:
            pass
        return HttpResponseRedirect("http://dailymails.org/mysite/")
    return HttpResponseRedirect("http://dailymails.org/")


@login_required
def my_plan(request):
    template_name = 'myadmin/payment/my_plan.html'
    return render(request, template_name, {})


@login_required
def features(request):
    #plantype = request.GET.get("plantype", 1)
    template_name = 'myadmin/payment/planfeatures_subscribers.html'
    return render(request, template_name, {})
