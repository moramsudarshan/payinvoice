from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from django.conf import settings




@login_required
def mysite(request):
    template_name = "myadmin/index.html"
    return render(request, template_name, {})
