jQuery.validator.addMethod("alphanumericonly", function(value, element) {
	return this.optional(element) || /^[a-z0-9]*$/i.test(value);
}, "Small Letters and numbers only please");



$("#userregistrationbutton").click(function(){

                $('#site-user-registration-form').validate({ // initialize the plugin
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        username: {
                            required: true,
                            alphanumericonly: true
                        },
                        mobile: {
                            required: true,
                            digits: true,
                            minlength : 4,
                            maxlength : 14
                        },
                        password: {
                            required: true,
                            minlength : 5
                        }
                    },
                    submitHandler: function (form) {

                    $('#userregistrationbutton').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: "/accounts/register/user/",
                           data: $("#site-user-registration-form").serialize(), // serializes the form's elements.
                           success: function(data)
                           {
                               if (data == 1)
                               {
                                   $('.alert-success').removeClass('hidden');
                                   $('.alert-info').addClass('hidden');                                   
                                   $('.form-control').val("");
				   setTimeout(function(){
			              document.location="/mysite/";
					}, 5000);
                               }
                               if (data == 2)
                               {
                                   $('.alert-info').removeClass('hidden');
                                   $('.alert-success').addClass('hidden');

                               }
                               $('#userregistrationbutton').removeAttr('disabled').html('Submit');

                           }
                         });
                    }
                });



            });

$("#subuser_button").click(function(){

                $('#subuserform').validate({ // initialize the plugin
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        username: {
                            required: true,
                            alphanumericonly: true
                        },
                        mobile: {
                            required: true,
                            digits: true,
                            minlength : 4,
                            maxlength : 14
                        },
                        password: {
                            required: true,
                            minlength : 5
                        }
                    },
                    submitHandler: function (form) {

                    $('#subuser_button').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: "/mysite/reseller/subaccounts/add/",
                           data: $("#subuserform").serialize(), // serializes the form's elements.
                           success: function(data)
                           {
                               if (data == 1)
                               {
                                   $('.alert-success').removeClass('hidden');
                                   $('.alert-info').addClass('hidden');                                   
                                   $('.form-control').val("");
				   setTimeout(function(){
			              document.location="/mysite/reseller/subaccounts/";
					}, 5000);
                               }
                               if (data == 2)
                               {
                                   $('.alert-info').removeClass('hidden');
                                   $('.alert-success').addClass('hidden');

                               }
                               $('#subuser_button').removeAttr('disabled').html('Submit');

                           }
                         });
                    }
                });



            });




$("#registrationbutton").click(function(){

                $('#site-registration-form').validate({ // initialize the plugin
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
			//skypeid: {
                        //    required: true,
			//    minlength : 5
                        //},
                        username: {
                            required: true,
                            alphanumericonly: true
                        },
                        mobile: {
                            required: true,
                            digits: true,
                            minlength : 4,
                            maxlength : 14
                        },
                        password: {
                            required: true,
                            minlength : 5
                        }
                    },
                    submitHandler: function (form) {

                    $('#registrationbutton').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: "/accounts/register/site/",
                           data: $("#site-registration-form").serialize(), // serializes the form's elements.
                           success: function(data)
                           {
                               if (data == 1)
                               {
                                  $('.alert-success').removeClass('hidden');
                                   $('.alert-info').addClass('hidden');
                                   site = "https://dailymails.org/mysite/";
                                   $(".alert-success a").attr("href", site );
                                   $(".alert-success a").html(site);
                                   $('.form-control').val("");
				                    setTimeout(function(){
					                    document.location=site;
					                }, 10000);


                               }
                               else if (data == 3)
                               {
                                  $('.alert-success').removeClass('hidden');
                                   $('.alert-info').addClass('hidden');
                                   site = "https://dailymails.org/mysite/affiliate/";
                                   $(".alert-success a").attr("href", site );
                                   $(".alert-success a").html(site);
                                   $('.form-control').val("");
				                    setTimeout(function(){
					                    document.location=site;
					                }, 10000);


                               }
                               else
                               {
                                   $('.alert-info').removeClass('hidden');
                                   $('.alert-success').addClass('hidden');

                               }
                               $('#registrationbutton').removeAttr('disabled').html('Register');

                           }
                         });
                    }
                });



            });


 $("#loginbutton").click(function(){

                $('#login-form').validate({ // initialize the plugin
                    rules: {

                        username: {
                            required: true
                        },

                        password: {
                            required: true,
                            minlength : 4
                        }
                    },
                    submitHandler: function (form) {

                    $('#loginbutton').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: "/accounts/login/",
                           data: $("#login-form").serialize(), // serializes the form's elements.
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 document.location="/mysite";

                               }
                               //if (data == 2)
                               else
                               {
                                   $('.alert-info').removeClass('hidden');


                               }
                               $('#loginbutton').removeAttr('disabled').html('Sign In');

                           }
                         });
                    }
                });



            });


 $("#campaign_button").click(function(){


                $('#campaignform').validate({ // initialize the plugin
                    rules: {

                        name: "required",
                        sender:"required",
                        template:"required",
                        contactlist:"required"
                        
                    },
                    submitHandler: function (form) {

                    $('#campaign_button').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: $(form).action,
                           data: $("#campaignform").serialize(), // serializes the form's elements.
                           dataType : 'json',
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 document.location="/mysite/campaign/";

                               }
                               else if (data == 5){
                                document.location="/";
                                
                               }
                               else
                               {
                                  $('.error-message').show();
                                   errorval=""
                                   for (var key in data) {
                                       errorval += key +" :  "+ data[key] + "<br>"
                                    }
                                   $('.error-message').html(errorval);

                               }
                               $('#campaign_button').removeAttr('disabled').html('Submit');

                           }


                         });
                    }
                });



 });


 $("#contact_button").click(function(){
                $('#contactform').validate({ // initialize the plugin
                    rules: {
                        uploaded_file:{
                            required:true,
                            extension: "xls"//"csv|xls|xlsx"
                        },
                        name: {
                            required: true,
                            minlength:5
                    }
                    },
                    submitHandler: function (form) {
                    var formData = new FormData(form);
                    $('#contact_button').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: $(form).action,
                           data: formData, // serializes the form's elements.
                           mimeType: "multipart/form-data",
                           contentType: false,
                           cache: false,
                           processData: false,
                           dataType : 'json',
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 document.location="/mysite/contact/";

                               }
                               else if (data == 5){
                                document.location="/";
                                
                               }
                               else
                               {
                                   $('.error-message').show();
                                   errorval=""
                                   for (var key in data) {
                                       errorval += key +" :  "+ data[key] + "<br>"
                                    }
                                   $('.error-message').html(errorval);

                               }
                               $('#contact_button').removeAttr('disabled').html('Submit');

                           }
                         });
                    }
                });



 });

 $("#contact_change_button").click(function(){
                $('#contact_change_form').validate({ // initialize the plugin
                    rules: {
                        uploaded_file:{
                            required:true,
                            extension: "xls"//"csv|xls|xlsx"
                        }
                    },
                    submitHandler: function (form) {
                    var formData = new FormData(form);
                    $('#contact_change_button').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: $(form).action,
                           data: formData, // serializes the form's elements.
                           mimeType: "multipart/form-data",
                           contentType: false,
                           cache: false,
                           processData: false,
                           dataType : 'json',
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 document.location="/mysite/contact/";

                               }
                               else if (data == 5){
                                document.location="/";
                                
                               }
                               else
                               {
                                   $('.error-message').show();
                                   errorval=""
                                   for (var key in data) {
                                       errorval += key +" :  "+ data[key] + "<br>"
                                    }
                                   $('.error-message').html(errorval);

                               }
                               $('#contact_change_button').removeAttr('disabled').html('Submit');

                           }
                         });
                    }
                });



 });



 $("#template_button").click(function(){
     CKEDITOR.instances.template_content.updateElement();
                $('#template_form').validate({ // initialize the plugin
                    ignore:[],
                    rules: {
                        template_name:"required",
                        emailcategory:"required",
                        /*template_image:{
                            required:function(element) {

                             if ($("#template_image_hidden").val()) {
                                return false;
                             }
                            else {
                                return true;
                            }
                                },
                            extension:"jpg|jpeg|png|pdf"
                        },
                        template_file:{
                            required: false,
                            extension:"htm|html"
                        },*/
                        template_content: {
                            required: true,
                            minlength:100
                    }
                    },
                    submitHandler: function (form) {
                        //var formData = new FormData(form);

                    $('#template_button').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: $(form).action,
                           data: $(form).serialize(), // serializes the form's elements.
                            //mimeType: "multipart/form-data",
                            //contentType: false,
                             //cache: false,
                            //processData: false,
                            dataType : 'json',
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 document.location="/mysite/templates/";

                               }
                               else if (data == 5){
                                document.location="/";
                                
                               }
                               else
                               {
                                   $('.error-message').show();
                                   errorval=""
                                   //data=JSON.parse(data)
                                   for (var key in data) {
                                       errorval += key +" :  "+ data[key] + "<br>"
                                    }
                                   $('.error-message').html(errorval);


                               }
                               $('#template_button').removeAttr('disabled').html('Submit');

                           }


                         });
                    }
                });



 });


 $("#btn_contact_submit").click(function(){

                $('#contact-form').validate({ // initialize the plugin
                    rules: {
                        email: {
                            required: true,
                            email: true
                        },
                        name: {
                            required: true
                        },
			//skypeid: {
		        //    required: true
			//    },
			whatsapp_number: {
			    required: true,
                            digits: true,
                            minlength : 4,
                            maxlength : 14
		    },
                        message: {
                            required: true,
                            minlength : 50
                        }
                    },
                    submitHandler: function (form) {

                    $('#btn_contact_submit').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: "/contactus/",
                           data: $("#contact-form").serialize(), // serializes the form's elements.
                           success: function(data)
                           {


                               if (data == 1)
                               {
                                  $('.alert-info').show();
                                   $('.alert-warning').hide();
                                   $('.form-control').val("");

                               }
                               //if (data == 2)
                               else
                               {
                                   $('.alert-warning').show();
                                   $('.alert-info').hide();

                               }
                               $('#btn_contact_submit').removeAttr('disabled').html('Send Message');

                           }
                         });
                    }
                });



            });

 $("#userprofiles_button").click(function(){

                $('#userprofile-form').validate({ // initialize the plugin
                    rules: {

                        first_name: {
                            required: true
                        },

                        mobile: {
                            required: true,
                            digits: true,
                            minlength : 4,
                            maxlength : 14
                        }
                    },
                    submitHandler: function (form) {

                    $('#userprofiles_button').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: "/accounts/",
                           data: $("#userprofile-form").serialize(), // serializes the form's elements.
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 document.location="/accounts";

                               }
                               else if (data == 5){
                                document.location="/";
                                
                               }
                               else
                               {
                                   $('#userprofile-form .alert-warning').removeClass('hidden');


                               }
                               $('#userprofiles_button').removeAttr('disabled').html('Submit');

                           }
                         });
                    }
                });



            });

 $("#companydetails_button").click(function(){

                $('#companydetails-form').validate({ // initialize the plugin
                    rules: {

                        company: {
                            required: true,
                            minlength:6
                        },

                        website: {
                            required: true,
                            minlength : 5
                        },
                        address: {
                            required: true,
                            minlength : 5
                        }
                    },
                    submitHandler: function (form) {

                    $('#companydetails_button').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: "/accounts/companydetails/",
                           data: $("#companydetails-form").serialize(), // serializes the form's elements.
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 document.location="/accounts/";

                               }
                               else if (data == 3){
                                   document.location="/";
                               }
                               else
                               {
                                   $('#companydetails-form .alert-warning').removeClass('hidden');


                               }
                               $('#companydetails_button').removeAttr('disabled').html('Submit');

                           }
                         });
                    }
                });



            });

 $("#changepassword_button").click(function(){

                $('#changepassword-form').validate({ // initialize the plugin
                    rules: {

                        new_password: {
                            required: true
                        },

                        confirm_password: {
                            equalTo: "#new_password"
                        }
                    },
                    submitHandler: function (form) {

                    $('#changepassword_button').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: "/accounts/change/password/",
                           data: $("#changepassword-form").serialize(), // serializes the form's elements.
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 document.location="/";

                               }
                               else if (data == 3){
                                   document.location="/";
                               }
                               else
                               {
                                   $('.alert-warning').removeClass('hidden');


                               }
                               $('#changepassword_button').removeAttr('disabled').html('Submit');

                           }
                         });
                    }
                });



            });


 $("#sender_button").click(function(){


                $('#senderform').validate({ // initialize the plugin
                    rules: {

                        name: "required",
                        from_name:"required",
                        domain:"required",
                        from_email:"required",
                        reply_to_email: {
                            required: true,
                            email: true
                        }

                    },
                    submitHandler: function (form) {

                    $('#sender_button').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: $(form).action,
                           data: $("#senderform").serialize(), // serializes the form's elements.
                           dataType : 'json',
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 document.location="/mysite/sender/";

                               }
                               else if (data == 5){
                                document.location="/";
                                
                               }
                               else
                               {
                                  $('.error-message').show();
                                   errorval=""
                                   for (var key in data) {
                                       errorval += key +" :  "+ data[key] + "<br>"
                                    }
                                   $('.error-message').html(errorval);

                               }
                               $('#sender_button').removeAttr('disabled').html('Submit');

                           }


                         });
                    }
                });



 });


 $("#domain_button").click(function(){


                $('#domainform').validate({ // initialize the plugin
                    rules: {
                        domain: "required"              

                    },
                    submitHandler: function (form) {

                    $('#domain_button').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: $(form).action,
                           data: $("#domainform").serialize(), // serializes the form's elements.
                           dataType : 'json',
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 document.location="/mysite/domain/";

                               }
                               else if (data == 5){
                                document.location="/";
                                
                               }
                               else
                               {
                                  $('.error-message').show();
                                   errorval=""
                                   for (var key in data) {
                                       errorval += key +" :  "+ data[key] + "<br>"
                                    }
                                   $('.error-message').html(errorval);

                               }
                               $('#domain_button').removeAttr('disabled').html('Submit');

                           }


                         });
                    }
                });



 });


  $("a.contact-validate").click(function(){
	  
	  confirm_val = confirm("Are you sure?It is paid features.");
	  if (confirm_val == false) {
		  return false;
	  }

        href=$(this).attr("href");
        obje=$(this)
        $(this).attr("href","#").html('Please Wait ...');
        $(this).unbind('click');
        $.ajax({
                           type: "GET",
                           url: href,
                           data: '',
                           dataType : 'json',
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                obje.html('Validating');
                                $(".contactlist-success span").html("List is Validating Now, It will take some times to complete. Please follow after some time.");

                               }
                             else if (data == 2){
                                $(".contactlist-success span").html("Your Email Validation Credit is less to validate this contact list. Please recharge your account or reduce contact list size");
                                
                               }
                               else if (data == 5){
                                
                                document.location="/";
                                
                               }
                               else
                               {

                                $(".contactlist-success span").html("Something wrong happen.");
                               }
                                $(".contactlist-success").show();

                           }


                         });

return false;



 });

  $("a.run-campaign").click(function(){
        href=$(this).attr("href");
        obje=$(this);
        $(this).attr("href","#").html('Please Wait ...');
        $(this).unbind('click');

        $.ajax({
                           type: "GET",
                           url: href,
                           data: '',
                           dataType : 'json',
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 obje.html('Running');
                                 $(".campaign-success span").html("Campaign is Running Now, It will take some times to complete. Please follow after some time.");



                               }
                             else if (data == 2){
                                $(".campaign-success span").html("Your Email Credit is very less to run this campaign. Please recharge your account or reduce associated contact list size");
                                
                               }
                            
                               else if (data == 5){
                                document.location="/accounts/signin/";                                
                               }
                               else if (data == 6){
                                document.location="/mysite/";                                
                               }
                               else
                               {
                                    $(".campaign-success span").html("Something wrong happen. Please try after sometime")

                               }
                               $(".campaign-success").show();

                           }


                         });
 return false;
  });

  $("a.send-test-email").click(function(){
        href=$(this).attr("href");
        obje=$(this);
        $(this).attr("href","#").html('Please Wait ...');
        $(this).unbind('click');

        $.ajax({
                           type: "GET",
                           url: href,
                           data: '',
                           dataType : 'json',
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 obje.html('Sent');
                                 $(".campaign-success span").html("Test Email is sent Now, It will take some times to reach your Inbox. Please check email after some time.");
				 


                               }

                               else if (data == 5){
                                document.location="/";

                               }
                               else
                               {
                                    $(".campaign-success span").html("Something wrong happen. Please try after sometime")

                               }
                               $(".campaign-success").show();

                           }


                         });
 return false;
  });

$("a.generate-report").click(function(){
        href=$(this).attr("href");
        obje=$(this);
        $(this).attr("href","#").html('Please Wait ...');
        $(this).unbind('click');

        $.ajax({
                           type: "GET",
                           url: href,
                           data: '',
                           dataType : 'json',
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 obje.html('Spam Check In Process');
                                 $(".spam-success span").html("Newsletter Spam Check is Processing Now, It will take some times to complete. Please follow after some time.");
				
				 setTimeout(function(){
					document.location="/mysite/newsletter-spam-tester/";
					}, 5000);


                               }
                             else if (data == 2){
                                $(".spam-success span").html("Your Credit is very less for Newsletter Spsm Check. Please recharge your account to proceed further");
                                
                               }
                            
                               else if (data == 5){
                                document.location="/";
                                
                               }
                               else
                               {
                                    $(".spam-success span").html("Something wrong happen. Please try after sometime")

                               }
                               $(".spam-success").show();

                           }


                         });
 return false;
  });

$("a.send-for-review").click(function(){
        href=$(this).attr("href");
        obje=$(this);
        $(this).attr("href","#").html('Please Wait ...');
        $(this).unbind('click');
        $.ajax({
                           type: "GET",
                           url: href,
                           data: '',
                           dataType : 'json',
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 obje.attr("href","/mysite/campaign/").html('Go To Campaign Page');
                                 $(".campaign-success span").html("Thank you for submitting the campaign for review. To see the status of its progress, Please visit campaign Listing Page.");
                               }
                                                        
                               
                               else
                               {
                                    $(".campaign-success span").html("Something wrong happen. Please try after sometime")

                               }
                               $(".campaign-success").show();

                           }


                         });
 return false;
  });
  
$("select.paymentduration").change(function(){
    document.location="?coupon_type=duration&value="+$(this).val();
});

$("select.country-based-price").change(function(){
    price_list = $(this).val().split("-");
    $("span#marketingplus").html(price_list[1]);
    $("span#transactionalplus").html(price_list[2]);
    $("span#otpplus").html(price_list[3]);
});

$("select.country-based-whatsapp-price").change(function(){
    price_list = $(this).val().split("-");
    $("span#templatedplus").html(price_list[1]);
});

$("button.couponbutton").click(function(){
    document.location="?coupon_type=couponcode&value="+$('#couponcode').val();
});




var counter=0;
var myJSON = '{"type1":{"eq":"=","neq":"!=","startswith":"Starts With", "endswith":"Ends With","contains":"Contains","not_contains":"Not Contains"}}';
var keyVar= '{"email": "type1","name":"type1"}';

function createOption(variable){
    var keyObj = JSON.parse(keyVar);
    var myObj = JSON.parse(myJSON);
    dictkey=keyObj[variable];
    data=myObj[dictkey];
    select='<select class="form-control"><option value="">--Condition--</option>';
    for(var index in data)
        {
            select += '<option value="'+index+'">'+data[index]+'</option>';
        }
    return select;

    }




 function addfields(flag){
  var newRow = $("<tr>");
        var cols = "";
        cols += '<td><div class="form-group"><select class="form-control"><option value="">--Condition--</option></select></div></td>';
        cols += '<td><div class="form-group"><input type="text" value="" class="form-control" /></div></td>';
        cols += '<td><div class="form-group"><input type="text" value="" class="form-control" /></div></td>';
        cols += '<td><div class="form-group"><input type="text" value="" class="form-control" /></div></td>';
        cols += '<td><div class="form-group"><input type="text" value="" class="form-control" /></div></td>';

        if (flag){
        cols += '<td><a href="#" class="ibtnDel"><i class="fa fa-trash-o text-danger"></i></a></td></tr>';
        }
        newRow.append(cols);
        $("table.table").append(newRow);
        counter++;
 }

 $(".addmore").click(function(){ 
     addfields(1);     
  });
 $("table").on("click", ".ibtnDel", function (event) {

        $(this).closest("tr").remove();
    });

$("table").on("change",".field", function (event) {

        $(this).closest("td").next('td').find('div').html(createOption($(this).val()));
    });

$("#datefilter").click(function(){
     if( !$('#fromdatepicker').val())
         {
             alert("Please choose From Date ");
             return;
         }
    else if ( !$('#todatepicker').val())
         {
             alert("Please choose To Date ");
             return;
         }
    else{
         document.location="?fromdate="+$('#fromdatepicker').val()+"&todate="+$('#todatepicker').val();
        }
        
});




 $("#web_push_button").click(function(){
                $('#webpushform').validate({ // initialize the plugin
                    rules: {

                        name: {
                            required: true,
                            maxlength: 56
                        },
                        domain: {
                            required: true,
                            maxlength: 56
                        },
                        // server_key:{
                        //     required:function(element) {

                        //      if ($("#fcmdetails").val() == 1) {
                        //         return false;
                        //      }
                        //     else {
                        //         return true;
                        //     }
                        //         }
                        // },
                        // senderid:{
                        //     required:function(element) {

                        //      if ($("#fcmdetails").val() == 1) {
                        //         return false;
                        //      }
                        //     else {
                        //         return true;
                        //     }
                        //         }
                        // },
                        icon:{
                            required: false,
                            extension:"png|jpg|jpeg",
                            // filesize: 1000,
                        }
                    },

                    submitHandler: function (form) {
                    var formData = new FormData(form);
                    $('#web_push_button').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: $(form).action,
                           data: formData, // serializes the form's elements.
                            mimeType: "multipart/form-data",
                            contentType: false,
                            cache: false,
                            processData: false,
                            dataType : 'json',
                           success: function(data)
                           {

                               if (data == 1)
                               {
                                 document.location="/mysite/push-notification/settings/";

                               }
                               else
                               {
                                   $('.error-message').show();
                                   errorval=""
                                //    data=JSON.parse(data)
                                   for (var key in data) {
                                       errorval += key +" :  "+ data[key] + "<br>"
                                    }
                                   $('.error-message').html(errorval);


                               }
                               $('#web_push_button').removeAttr('disabled').html('Submit');

                           }
                         });
                    }
                });



 });
 $("#ssp_button").click(function(){
    $('#ssp_form').validate({ // initialize the plugin
        rules: {

            name: {
                required: true,
                maxlength: 56
            },
        },

        submitHandler: function (form) {
        var formData = new FormData(form);
        $('#ssp_button').attr('disabled', 'disabled').html('Please Wait ...');
            $.ajax({
               type: "POST",
               url: $(form).action,
               data: formData, // serializes the form's elements.
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                dataType : 'json',
               success: function(data)
               {

                   if (data == 1)
                   {
                     document.location="/mysite/sms-notification/settings/";

                   }
                   else
                   {
                       $('.error-message').show();
                       errorval=""
                    //    data=JSON.parse(data)
                       for (var key in data) {
                           errorval += key +" :  "+ data[key] + "<br>"
                        }
                       $('.error-message').html(errorval);


                   }
                   $('#ssp_button').removeAttr('disabled').html('Submit');

               }
             });
        }
    });



});
$("#btn_email_submit").click(function(){
    $('#email_form').validate({ // initialize the plugin
        rules: {

            company_name: {
                required: true,
                maxlength: 56
            },
        },

        submitHandler: function (form) {
        var formData = new FormData(form);
        $('#btn_email_submit').attr('disabled', 'disabled').html('Please Wait ...');
            $.ajax({
               type: "POST",
               url: $(form).action,
               data: formData, // serializes the form's elements.
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                dataType : 'json',
               success: function(data)
               {

                   if (data == 1)
                   {
                     document.location="/mysite/email-notification/settings/";

                   }
                   else
                   {
                       $('.error-message').show();
                       errorval=""
                    //    data=JSON.parse(data)
                       for (var key in data) {
                           errorval += key +" :  "+ data[key] + "<br>"
                        }
                       $('.error-message').html(errorval);


                   }
                   $('#btn_email_submit').removeAttr('disabled').html('Submit');

               }
             });
        }
    });



});

  $("#template_push_button").click(function(){
                $('#template_push_form').validate({ 
                    rules: {
                        template_name:"required",
                        emailcategory:"required",
                       
                        template_content: {
                            required: true,
                            maxlength:125,
                            minlength:25
                    }
                    },
                    submitHandler: function (form) {
                    $('#template_push_button').attr('disabled', 'disabled').html('Please Wait ...');
                        $.ajax({
                           type: "POST",
                           url: $(form).action,
                           data: $(form).serialize(), 
                           dataType : 'json',
                           success: function(data)
                           {
                               if (data == 1)
                               {
                                 document.location="/mysite/templates/";

                               }
                               else if (data == 5){
                                document.location="/";
                                
                               }
                               else
                               {
                                   $('.error-message').show();
                                   errorval=""
                                   for (var key in data) {
                                       errorval += key +" :  "+ data[key] + "<br>"
                                    }
                                   $('.error-message').html(errorval);


                               }
                               $('#template_push_button').removeAttr('disabled').html('Submit');

                           }


                         });
                    }
                });



 });

 $("#template_sms_button").click(function(){
    $('#template_sms_form').validate({ 
        rules: {
            template_name:"required",
            emailcategory:"required",
           
            template_content: {
                required: true,
                maxlength:128,
                minlength:25
        }
        },
        submitHandler: function (form) {
        $('#template_sms_button').attr('disabled', 'disabled').html('Please Wait ...');
            $.ajax({
               type: "POST",
               url: $(form).action,
               data: $(form).serialize(), 
               dataType : 'json',
               success: function(data)
               {
                   if (data == 1)
                   {
                     document.location="/mysite/templates/";

                   }
                   else if (data == 5){
                    document.location="/";
                    
                   }
                   else
                   {
                       $('.error-message').show();
                       errorval=""
                       for (var key in data) {
                           errorval += key +" :  "+ data[key] + "<br>"
                        }
                       $('.error-message').html(errorval);


                   }
                   $('#template_sms_button').removeAttr('disabled').html('Submit');

               }


             });
        }
    });



});