from django.contrib import admin
from .models import *


class PaymentTrackAdmin(admin.ModelAdmin):
    list_display = ('user', 'payment_id', 'amount', 'duration', 'amount_received', 'status', 'source', 'rate', 'plan',
                    'added_date_time', 'payment_date_time')


class PaymentPlanAdmin(admin.ModelAdmin):
    list_display = ('name', 'plan_type', 'price', 'count', 'active', 'report_access')
    list_editable = ('active',)
    list_filter = ('plan_type', 'active')


admin.site.register(PaymentTrack, PaymentTrackAdmin)
admin.site.register(PaymentPlan, PaymentPlanAdmin)
