from django.contrib import admin
from .models import *


class SupportPlanCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'active', 'modified_date', 'date')


class SupportCasesDetailInline(admin.TabularInline):
    model = SupportCasesDetail


class SupportCasesAdmin(admin.ModelAdmin):
    list_display = (
    'subject', 'severity', 'status', 'supportcategory', 'product', 'active', 'user', 'description', 'modified_date',
    'date')
    list_filter = ('severity', 'status', 'supportcategory__name', 'user__username')
    inlines = [SupportCasesDetailInline, ]


admin.site.register(SupportPlanCategory, SupportPlanCategoryAdmin)
admin.site.register(SupportCases, SupportCasesAdmin)
