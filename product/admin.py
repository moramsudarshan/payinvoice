from django.contrib import admin
from .models import *


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'active', 'is_addon', )
    list_editable = ('active',  'is_addon')


admin.site.register(Product, ProductAdmin)
