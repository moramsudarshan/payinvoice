from django.conf.urls import url
from .views import *


urlpatterns = [
    url(r'^product/add/$', product_change, name="add"),
    url(r'^product/update/(?P<slug>.+)/$', product_change, name="update"),
    url(r'^product/delete/(?P<pk>\d+)/$', delete_page, name="delete"),
    url(r'^product/detail/(?P<slug>.+)/$', detail_page, name="detail"),
    url(r'^product/$', list_page, name="listing"),


    url(r'^customer/add/$', customer_change, name="add_customer"),
    url(r'^customer/$', customer_list, name="listing"),
    url(r'^customer/detail/(?P<pk>\d+)/$', customer_detail, name="detail_customer"),
    url(r'^customer/delete/(?P<pk>\d+)/$', customer_delete, name="delete_customer"),
    url(r'^customer/update/(?P<slug>.+)/$', customer_change, name="update_customer"), 

    url(r'^invoice/$', invoice, name='invoice_list'),
    url(r'^invoice/create/$', invoice_change, name='invoice_create'),
    url(r'^invoice/update/(?P<pk>\d+)/$', invoice_change, name='update_invoice'),
    url(r'^invoice/detail/(?P<pk>\d+)/$', invoice_detail, name='detail_invoice'),
    url(r'^invoice/delete/(?P<pk>\d+)/$', invoice_delete, name='delete_invoice'),

]
