from django.contrib import admin
from .models import *


class CountryAdmin(admin.ModelAdmin):
    list_display = (
    'name', 'idd', 'ccd', 'iso_code', 'active', 'user', 'added_date', 'modified_date',)
    list_filter = ('active',)
    exclude = ('user',)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()




class ContactUsAdmin(admin.ModelAdmin):
    list_display = ('name', 'added_date', 'email', 'skypeid', 'country_detail', 'mobile', 'message', 'page', 'status')
    list_editable = ('status',)
    list_filter = ('page',)

    def country_detail(self, obj):
        if obj.country:
            return obj.country.name + "-idd-%s, ccd-+%s" % (obj.country.idd, obj.country.ccd)
        else:
            return ''





admin.site.register(Country, CountryAdmin)
admin.site.register(ContactUs, ContactUsAdmin)

