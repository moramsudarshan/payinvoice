from django import forms
from .models import SupportCases


class SupportCasesForm(forms.ModelForm):
    class Meta:
        model = SupportCases
        exclude = ('user', 'status', 'severity')