from django.db import models
from django.utils.translation import ugettext_lazy as _
from mailprocess.models import ModelBase


class Product(ModelBase):
    name = models.CharField(max_length=100, null=True, blank=True)

    active = models.BooleanField(default=0)
    is_addon = models.BooleanField(default=0)
    def __str__(self):
        return str(self.name)
