from django.conf.urls import url
from django.views.generic import TemplateView
from .views import *

urlpatterns = [
    url(r'^$', home, name="homepage"),
    url(r'^contactus/$', contactus, name="contactus"),
    url('^robots.txt$', TemplateView.as_view(template_name="static/robots.txt")),
    url('^sitemap.xml$', TemplateView.as_view(template_name="static/sitemaps.xml")),

    #url(r'^sitemap/$', htmlsitemap, name='htmlsitemap'),
]
