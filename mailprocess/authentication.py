from django.contrib.auth.models import User
from rest_framework import authentication
from rest_framework import exceptions

from accounts.models import UserProfile


class APIAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        # apikey = request.META.get('HTTP_APIKEY')
        #apikey = request.GET('api_key')

        apikey = '89ca0babc570e9354b8251'
        if not apikey:
            return None
        try:
            userprofile = UserProfile.objects.get(key=apikey)
        except User.DoesNotExist:
            raise exceptions.AuthenticationFailed('No such user')
        return userprofile.user, None
