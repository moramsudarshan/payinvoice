from django.urls import reverse
from django.db import models
from django.db.models.base import ModelBase
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from mailprocess.models import *

COMPOSITE_SCHEME_CHOICES = ((0,'1% (For Traders and Manufacturers)'),(1,'5% (For Restaurant sector)'),
(2,'6% (For Suppliers of Services or Mixed Suppliers)'))
CURRENCY_CODE_CHOICES = ((0,'USD'), (1,'EUR'))
CURRENCY_SYMBOL_CHOICES = ((0, 'USD $'), (1, 'EUR €'))
DECIMAL_PLACES_CHOICES=((0, '0'), (1,'1'), (2,'2'))
FORMAT_CHOICES= ((0,'12345678'),(1,'12345678'))
TEMPLATE_CHOICES= ((0,'template'),(1,'background'))



class TaxRate(ModelBase):
    tax_name = models.CharField(max_length=50)
    rate  = models.DecimalField(max_digits=9, decimal_places=2)
    slug = models.SlugField(max_length=255, unique=True)
    def __str__(self):
        return str(self.tax_name)

    def save(self, *args, **kwargs):
        if not self.id:
            super(TaxRate, self).save(*args, **kwargs)
        self.slug = slugify(self.tax_name) + "-" + \
            str(self.user.id) + "-" + str(self.id)
        super(TaxRate, self).save(*args, **kwargs)

class GstSetting(ModelBase):
    gstin = models.CharField(max_length=15)
    gstdate = models.DateField()
    compositionscheme = models.IntegerField(choices=COMPOSITE_SCHEME_CHOICES,  default=0)
    slug = models.SlugField(max_length=255, unique=True)
    def __str__(self):
        return str(self.gstin)

    def save(self, *args, **kwargs):
        if not self.id:
            super(GstSetting, self).save(*args, **kwargs)
        self.slug = slugify(self.gstdate) + "-" + \
            str(self.user.id) + "-" + str(self.id)
        super(GstSetting, self).save(*args, **kwargs)
    

    

class Currency(ModelBase):
    currency_code = models.IntegerField(choices=CURRENCY_CODE_CHOICES,  default=0)
    currency_symbol = models.IntegerField(choices=CURRENCY_SYMBOL_CHOICES,  default=0)
    decimal_places = models.IntegerField(choices=DECIMAL_PLACES_CHOICES,  default=0)
    choose_format =  models.IntegerField(choices=FORMAT_CHOICES,  default=0)
    slug = models.SlugField(max_length=255, unique=True)

    def __str__(self):
        return str(self.currency_code)

    def save(self, *args, **kwargs):
        if not self.id:
            super(Currency, self).save(*args, **kwargs)
        self.slug = slugify(self.choose_format) + "-" + \
            str(self.user.id) + "-" + str(self.id)
        super(Currency, self).save(*args, **kwargs)

class Template(ModelBase):
    name=models.CharField(max_length=100)
    template_type = models.IntegerField(choices=TEMPLATE_CHOICES,  default=0)
    preview_image=models.ImageField(upload_to='uploads/invoice/', null=True, blank=True)
    path = models.CharField(max_length=100, null=True, blank=True)
    thumbnail=models.ImageField(upload_to='uploads/invoice/', null=True, blank=True)

    def __str__(self):
        return str(self.name)



class UserTemplate(models.Model):
    template=models.ForeignKey(Template,on_delete=models.CASCADE)
    customer= models.ForeignKey(User, on_delete=models.CASCADE, related_name="template_user")

   
    def __str__(self):
        return str(self.customer)

class NotesAndConditions(ModelBase):
    customer_notes = models.TextField()
    terms_and_conditions = models.TextField()

    def __str__(self):
        return self.customer_notes
