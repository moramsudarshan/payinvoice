from django import forms
from .models import *
from .models import Products, Customers

class ProductInvoiceForm(forms.ModelForm):
    class Meta:
        model = ProductInvoice
        exclude = ('user', 'severity', 'slug')


class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoices
        exclude = ('user', 'active', 'severity',)


class ProductForm(forms.ModelForm):
    class Meta:
        model = Products
        exclude = ('user', 'slug', 'status', 'severity', 'active')


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customers
        exclude = ('user', 'slug', 'status', 'severity', 'active')

