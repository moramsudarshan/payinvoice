"""
Django settings for bck project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
# from .local_settings import *
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

ADMINS = [('RegalAbode', 'regalabode@gmail.com'), ]
SERVER_EMAIL = "sales@automate.org"
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'v!o3=n-+zqdu1kp%@utrck^(_*e^m$j%l$ck35y5h!s@zg5d0z'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'mailprocess',
    'product',
    'accounts',
    'support',
    'payment',
    'coupons',
    'myadmin',
    'rest_framework',
    'invoice',
    'invoice_settings',
    'api',
)

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

]

ROOT_URLCONF = 'payinvoice.urls'

WSGI_APPLICATION = 'payinvoice.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'sinvoice',
        'USER': 'root',
        'PASSWORD': 'root',
    }
}


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images) ###
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATIC_ROOT = os.path.join(BASE_DIR, 'staticcollection')
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static")
]
STATIC_URL = '/static/'

# TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates'),os.path.join(BASE_DIR, 'media'),]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIA_URL = '/media/'
HTML_EXT = ('html', 'htm')
HTML_CONTENT_TYPE = ('text/html',)
HTML_SIZE = 10 * 1024 * 1024
IMAGE_SIZE = 10 * 1024 * 1024
SITE_DOMAIN = ['http://dailymails.org', ]

# SESSION_COOKIE_DOMAIN = ".automate.org"
DEFAULT_SITE_EMAIL = 'DailyMails Team <sales@automate.org>'
DEFAULT_FROM_EMAIL = 'Server <admin@automate.org>'
FROM_EMAIL = ['regalabode@gmail.com',
              'kamta.singh@gmail.com', 'kamta.singhpython@gmail.com']

FILE_UPLOAD_PERMISSIONS = 0o755
DATA_UPLOAD_MAX_NUMBER_FIELDS = None
LOGIN_URL = "/accounts/signin/"
TIME_PERIOD_IN_SECONDS = 90 * 24 * 60 * 60

# Logging settings


LOGFILE_SIZE = 10 * 1024 * 1024
LOGFILE_COUNT = 10
SITE_ID = 1
# LOG_BASE_DIRECTORY = '/var/log/automate/'

DURATION_DISCOUNT = {"1": 0, "2": 30, "3": 40}
'''LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format': '%(asctime)s | %(levelname)s | %(module)s [%(process)d %(thread)d] | [%(filename)s:%(lineno)s - %(funcName)s() ] | \n%(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOG_BASE_DIRECTORY + '/automate.log',
            'maxBytes': LOGFILE_SIZE,
            'backupCount': LOGFILE_COUNT,
            'formatter': 'verbose'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django': {
            'handlers': ['file', 'mail_admins'],
            'level': 'INFO',
            'propagate': True,
        },
        'smtp': {
            'handlers': ['file', 'mail_admins'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'contactlist_manager': {
            'handlers': ['file', 'mail_admins'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}'''

REST_FRAMEWORK = {

    'DEFAULT_AUTHENTICATION_CLASSES': [
        'mailprocess.authentication.APIAuthentication', ],

    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),

}

# ------------email setting---------------------#
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = None
EMAIL_HOST_USER = None
EMAIL_HOST_PASSWORD = None
EMAIL_PORT = None
EMAIL_USE_TLS = None

# -------x-----------email setting -------------x ---------
