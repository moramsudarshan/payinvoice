from django.conf.urls import url
from django.views.generic import TemplateView
from .views import *

urlpatterns = [
    url(r'^offers/$', offers, name="offers"),
    url(r'^mysite/offers/$', myoffers, name="myoffers"),
]
