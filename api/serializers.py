from rest_framework import serializers
from invoice.models import Products, Customers, Invoices, ProductInvoice
from django.utils.text import slugify

class ProductsSerializers(serializers.ModelSerializer):
    class Meta:
        model = Products
        exclude = ['slug', 'user', ]


class ProductInvoiceSerializers(serializers.ModelSerializer):
    products = serializers.CharField(source='products.name')
    products_id = serializers.CharField(source='products.id')

    class Meta:
        model = ProductInvoice
        exclude = ['slug', 'user', 'active', 'invoices', ]


class InvoicesSerializers(serializers.ModelSerializer):
    invoproducts = ProductInvoiceSerializers(many=True)

    class Meta:
        model = Invoices
        fields = ['invoice_number', 'order_number', 'invoice_date', 'due_date',
                  'customer', 'subtotal', 'total', 'invoproducts', ]

        def create(self, validated_data):
            print('---------------------------------------------------->')
            print('---------------------------------------------------->')
            print('---------------------------------------------------->')
            print('---------------------------------------------------->')
            print(validated_data)
            products = validated_data.pop('invoproducts')
            # invoproducts = []
            invoice = Invoices.objects.create(**validated_data)
            print(products)
            print(invoice.user)
            for p in products:
                # p_instance, created = ProductInvoice.objects.get_or_create(name=p)
                # invoproducts += [p_instance]
                ProductInvoice.objects.create(**p, user=invoice.user, invoices=invoice)
                print(invoice)
                # Print(validated_data)
                # invoice.invoproducts.set(invoproducts)
                # invoice.save()
            return invoice

            # invoproducts = ProductInvoiceSerializers.create(ProductInvoiceSerializers(),
            # validated_data=invoproducts_data)
            # invoices = Invoices.objects.create(**validated_data, invoproducts=invoproducts,)
            # subject_major=validated_data.pop('subject_major'))
            # return invoices


class NestedProductInvoiceSerializers(serializers.ModelSerializer):
    # products = serializers.CharField(source='products.name')
    # products_id = serializers.CharField(source='products.id')

    class Meta:
        model = ProductInvoice
        exclude = ["active", "invoices", "slug"]
        read_only_fields = ("id", "active", "invoices")


class NestedInvoicesSerializers(serializers.ModelSerializer):
    productinvoices = NestedProductInvoiceSerializers(many=True)

    class Meta:
        model = Invoices
        exclude = ["pdf_code"]
        read_only_fields = ("id", "added_date", "modified_date", "active",)

    def create(self, validated_data):
        productinvoices_data = validated_data.pop("productinvoices")
        invoice = Invoices.objects.create(**validated_data)
        for productinvoice_data in productinvoices_data:
            p_obj = ProductInvoice.objects.create(invoices=invoice,**productinvoice_data)
            p_id = ProductInvoice.objects.get(id=p_obj.id)
            print("pr_id", p_id)
            p_id.slug = slugify(p_id.amount) + "-" + str(p_id.invoices.user.id) + "-" + str(p_id.id) + "-" + str(p_id.invoices.customer.id)
            p_id.save()
        return invoice

    def update(self, instance, validated_data):
        productinvoices_data = validated_data.pop("productinvoices")
        productinvoices = (instance.productinvoices).all()
        productinvoices = list(productinvoices)

        instance.customer =  validated_data.get('customer',instance.customer)
        instance.invoice_number =  validated_data.get('invoice_number',instance.invoice_number)
        instance.order_number =  validated_data.get('order_number',instance.order_number)
        instance.invoice_date =  validated_data.get('invoice_date',instance.invoice_date)
        instance.terms =  validated_data.get('terms',instance.terms)
        instance.due_date =  validated_data.get('due_date',instance.due_date)
        instance.subject =  validated_data.get('subject',instance.subject)
        instance.subtotal =  validated_data.get('subtotal',instance.subtotal)
        instance.total =  validated_data.get('total',instance.total)
        instance.pdf_code =  validated_data.get('pdf_code',instance.pdf_code)
        instance.status =  validated_data.get('status',instance.status)
        instance.save()

        for productinvoice_data in productinvoices_data:
            productinvoice = productinvoices.pop(0)
            productinvoice.products = productinvoice_data.get("products", productinvoice.products)
            productinvoice.quantity = productinvoice_data.get("quantity", productinvoice.quantity)
            productinvoice.rate = productinvoice_data.get("rate", productinvoice.rate)
            productinvoice.discount = productinvoice_data.get("discount", productinvoice.discount)
            productinvoice.discount_unit = productinvoice_data.get("discount_unit", productinvoice.discount_unit)
            productinvoice.amount = productinvoice_data.get("amount", productinvoice.amount)
            productinvoice.slug = slugify(productinvoice.amount) + "-" + str(productinvoice.invoices.user.id) + \
                                  "-" + str(productinvoice.id) + "-" + str(productinvoice.invoices.customer.id)
            productinvoice.save()
        return instance


class CustomersSerializers(serializers.ModelSerializer):
    class Meta:
        model = Customers
        fields = ['first_name', 'last_name', 'phone', 'id', 'user', 'active', ]


class DownloadInvoicesSerializers(serializers.ModelSerializer):
    class Meta:
        model = Invoices
        fields = ['pdf_code', ]
