#from mail.tasks import sendemail, sendverificationemail
from django.template import loader
from django.core import signing
from django.contrib.auth.models import User
from django.contrib.auth import get_backends
from accounts.models import UserUsage, UserProfile
from product.models import Product
import decimal
import pytz
from datetime import datetime
from django.db.models import Q


def send_site_registration_mail(user, site):
    subject = "Verification mail from dailymails.org"
    from_email = 'dailymails.org<sales@dailymails.org>'
    to_email = user.email
    userobj = encryptdata({'email': user.email, 'username': user.username})
    message = loader.render_to_string("mail/site_verification_mail.html",
                                      {'user': user, 'site': site, 'userobj': userobj})
    sendverificationemail(subject=subject, message=message, from_email=from_email, to_email=to_email,
                          htmlmessage=message)


def encryptdata(val):
    return signing.dumps(val)


def decryptdata(val):
    return signing.loads(val)



def authenticate_fb(username):
    """
    If the given credentials are valid, return a User object.
    """
    try:

        user = User.objects.get(username=username)
        for backend in get_backends():
            user.backend = "%s.%s" % (backend.__module__, backend.__class__.__name__)
        return user
    except User.DoesNotExist:
        return None


def site_usage(rows, product_id, userdetail):
    product = Product.objects.get(pk=product_id)
    try:
        if product.id == 1:
            if userdetail_obj.myplan.plan_type == 2:
                price = decimal.Decimal('0.00')
            else:
                price = decimal.Decimal(rows * userdetail.myplan.unit_price())
        else:
            price = rows * (product.price) / product.quantity
    except:
        price = decimal.Decimal('0.00')

    if price < 0.01 and product.id != 5:
        price = decimal.Decimal('0.01')
    usage = UserUsage(userusage=userdetail, product=product, quantity=rows, price=price)
    usage.save()
    userdetail_obj = UserProfile.objects.get(id=userdetail.id)
    userdetail_obj.remaining_amount -= price
    userdetail_obj.used_amount += price
    if product.id == 1 and userdetail_obj.myplan.plan_type == 1:
        userdetail_obj.remaining_emails -= rows
        userdetail_obj.sent_emails += rows
    userdetail_obj.save()


def check_credit(rows, product_id, userdetail):
    if product_id == 1:
        userdetail_obj = UserProfile.objects.get(id=userdetail.id)
        if userdetail.myplan.plan_type == 1:
            if userdetail.remaining_days() < 1 or userdetail.remaining_emails < rows:
                return True
            return False
        elif userdetail.myplan.plan_type == 2:
            if userdetail.remaining_days() < 1 or userdetail.remaining_emails < 0:
                return True
            return False
        return False
    return False


def asia_pacific_time(datepicker, timepicker):
    format = "%Y-%m-%d %H:%M:%S"
    time = datepicker + " " + timepicker + ":00"
    return time
    # datetime_obj=datetime.strptime(time,"%Y-%m-%d %H:%M:%S")
    # datetime_obj_zone=datetime_obj.replace(tzinfo=pytz.timezone(time_zone))
    # asia=datetime_obj_zone.astimezone(pytz.timezone('UTC'))
    # asia_str=datetime_obj_zone.strftime(format)
    # return datetime_obj_zone
    # return datetime.strptime(asia_str,format)#.replace(tzinfo=pytz.timezone('Asia/Kolkata'))


def zone_pacific_time(schduled_on, time_zone):
    format = "%Y-%m-%d %H:%M:%S"
    # datetime_obj_zone=schduled_on.replace(tzinfo=pytz.timezone('Asia/Kolkata'))
    asia = schduled_on.astimezone(pytz.timezone(time_zone))
    asia_str = asia.strftime(format)
    return datetime.strptime(asia_str, format)


def authenticate_api(username, key):
    """
    If the given credentials are valid, return a User object.
    """
    try:

        userprofile = UserProfile.objects.get(Q(key=key), Q(user__username=username) | Q(user__email=username))
        return userprofile.user
    except UserProfile.DoesNotExist:
        return None
    except:
        return None


def check_api_credit(rows, userdetail):
    if userdetail.api_remaining_days() < 1 or userdetail.remaining_api_emails < rows:
        return True
    return False


def site_api_usage(rows, product_id, userid):
    userdetail = UserProfile.objects.get(user__id=userid)
    product = Product.objects.get(pk=product_id)
    price = decimal.Decimal(rows * userdetail.apiplan.unit_price())
    usage = UserUsage(userusage=userdetail, product=product, quantity=rows, price=price)
    usage.save()
    userdetail.remaining_amount -= price
    userdetail.used_amount += price
    if product.id == 2:
        userdetail.remaining_api_emails -= rows
        userdetail.sent_api_emails += rows
    userdetail.save()
