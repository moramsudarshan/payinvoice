from django.contrib import admin
from .models import *


class CouponAdmin(admin.ModelAdmin):
    list_display = (
    'coupon_code', 'discount', 'expires_on', 'user', 'coupon_type', 'plan_or_category', 'condition', 'active',
    'added_date', 'modified_date')
    list_filter = ('active',)


class CouponUsageAdmin(admin.ModelAdmin):
    list_display = ('coupon', 'payment', 'active', 'user', 'added_date', 'modified_date')
    list_filter = ('active',)
    exclude = ('user',)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


admin.site.register(Coupon, CouponAdmin)
admin.site.register(CouponUsage, CouponUsageAdmin)
