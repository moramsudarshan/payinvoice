from django.conf.urls import url
from .views import *

urlpatterns = [
    
    url(r'^invoicesetting/tax/$', tax, name='tax_list'),
    url(r'^invoicesetting/tax/create/$', tax_change, name='tax_create'),
    url(r'^invoicesetting/tax/edit/(?P<slug>.+)/$', tax_change, name='edit_tax'),
    url(r'^invoicesetting/tax/delete/(?P<pk>\d+)/$', tax_delete, name='tax_delete'),
    
    
    url(r'^invoicesetting/currency/$', currency, name='currency_list'),
    url(r'^invoicesetting/currency/create/$', currency_change, name='currency_create'),
    url(r'^invoicesetting/currency/edit/(?P<slug>.+)/$', currency_change, name='edit_currency'),
    url(r'^invoicesetting/currency/delete/(?P<pk>\d+)/$', currency_delete, name='currency_delete'),




    url(r'^invoicesetting/gst/$', gst, name='gst_list'),
    url(r'^invoicesetting/gst/create/$', gst_change, name='gst_create'),
    url(r'^invoicesetting/gst/edit/(?P<slug>.+)/$', gst_change, name='edit_gst'),
    url(r'^invoicesetting/gst/delete/(?P<pk>\d+)/$', gst_delete, name='gst_delete'),
    url(r'^invoicesetting/(?P<slug>.+)/add/$', invoice_template_add, name='invoice_template_add'),
    url(r'^invoicesetting/(?P<slug>.+)/$', invoice_template, name='invoice_template'),

]