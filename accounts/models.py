from django.db import models
from django.contrib.auth.models import User
from mailprocess.models import Country, ModelBase
from django.utils.translation import ugettext_lazy as _
from product.models import Product
from payment.models import PaymentPlan
from django.utils import timezone

STATUS_CHOICE = ((1, "Registration Completed"), (2, "Accounts Verified"),
                 (3, "Account UnderReview"), (4, "Account Activated"))
PAYMENT_CHOICE = (('1', "Pending Payment"), ('2', "Verified"))
USER_TYPE = ((1, "Normal Users"), (2, "Reseller Users"), (3, "Reseller Admins"), (4, "Affiliate User"))


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="userdetail")
    mobile = models.BigIntegerField()
    company = models.CharField(max_length=100, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    country_detail = models.ForeignKey(Country, on_delete=models.CASCADE, null=True, blank=True)
    website = models.CharField(_('domain name'), null=True, blank=True, max_length=100)
    userstatus = models.IntegerField(choices=STATUS_CHOICE, default=1)

    total_subscribers = models.IntegerField(default=0)
    remaining_subscribers = models.IntegerField(default=0)

    account_type = models.IntegerField(choices=USER_TYPE, default=1)
    skypeid = models.CharField(max_length=100, null=True, blank=True)

    myplan = models.ForeignKey(PaymentPlan, on_delete=models.CASCADE, null=True, blank=True, related_name='myplan')
    valid_date = models.DateTimeField(null=True, blank=True)

    supportplan = models.ForeignKey(PaymentPlan, on_delete=models.CASCADE, null=True, blank=True,
                                    related_name='supportplan')
    supportplan_valid_date = models.DateTimeField(null=True, blank=True)

    key = models.CharField(max_length=30, null=True, blank=True)
    is_api_enabled = models.BooleanField(default=False)

    def __str__(self):
        return str(self.user.username)

    def remaining_days(self):
        if self.myplan.id == 1:
            return 30
        elif self.myplan.id != 1 and self.valid_date:
            noofdays = self.valid_date - timezone.now()
            if noofdays.days == 0:
                return 1
            elif noofdays.days < 0:
                return 0
            else:
                return noofdays.days + 1
        else:
            return 0

    def support_remaining_days(self):
        if self.supportplan.id != 2 and self.supportplan_valid_date:
            noofdays = self.supportplan_valid_date - timezone.now()
            if noofdays.days == 0:
                return 1
            elif noofdays.days < 0:
                return 0
            else:
                return noofdays.days + 1
        else:
            return 0


class UserUsage(models.Model):
    userusage = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name="usrusage")
    product = models.ForeignKey(Product, on_delete=models.CASCADE, )
    quantity = models.IntegerField(default=0)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    active = models.BooleanField(default=True)
    date = models.DateTimeField(auto_now_add=True)


class Broadcast(ModelBase):
    message = models.TextField()

    def __str__(self):
        return str(self.id)
