from django.shortcuts import render, redirect
from django.shortcuts import render, get_object_or_404
from .forms import *
from .models import *
from django.contrib import messages
from django.views.generic.edit import UpdateView
from django.http import HttpResponse, HttpResponseRedirect

from .forms import ProductForm, CustomerForm, ProductInvoiceForm
from .models import Products, PRODUCT_TYPE_CHOICES, UNIT_CHOICES, Customers, CUSTOMER_CHOICE, CURRENCY_CHOICES, \
    PAYMENT_TERMS_CHOICE, LANGUAGE_CHOICES, STATUS_CHOIES
from mailprocess.models import Country
from invoice_settings.models import NotesAndConditions




def invoice(request):
    template_name = "myadmin/invoice/invoicelist.html"
    qs = Invoices.objects.filter(user=request.user, active= True)
    invoice_number_query = request.GET.get('invoice_number')
    invoice_date_query = request.GET.get('invoice_date')
    due_date_query = request.GET.get('due_date')

    if invoice_number_query != "" and  invoice_number_query is not None:
        qs = qs.filter(invoice_number__iexact=invoice_number_query)

    elif invoice_date_query != "" and  invoice_date_query is not None:
        qs = qs.filter(invoice_date__iexact=invoice_date_query)

    elif due_date_query != "" and  due_date_query is not None:
        qs = qs.filter(due_date__iexact=due_date_query)    

    context = {
        'inv': qs
    }   
    return render(request, template_name, context )


# def invoice_change(request, slug=None):
#     invoicedetails=None
#     if slug:
#         invoicedetails=ProductInvoice.objects.get(user=request.user, slug= slug)
#     if request.method == "POST":
#         form = ProductInvoiceForm(request.POST, instance=invoicedetails)
#         if form.is_valid():
#             new_upload = form.save(commit=False)
#             new_upload.user = request.user
#             new_upload.active = 1
#             new_upload.severity = 1
#             new_upload.status = 1
#             new_upload.save()
#             return HttpResponseRedirect("/mysite/mysite/invoice/")
#     template_name = "myadmin/invoice/add-invoice.html"
#     return render(request, template_name, {'invoicedetails':invoicedetails})

def invoice_change(request, pk=None):
    invoice_data = None
    pinvoice_data = None
    note_condition = NotesAndConditions.objects.get(user=request.user)

    if pk:
        invoice_data = Invoices.objects.get(user=request.user, id=pk)
    if invoice_data:
        pinvoice_data = ProductInvoice.objects.filter(invoices=invoice_data)

    if request.method == "POST":
        invoice_form = InvoiceForm(request.POST, instance=invoice_data)
        pinvoice_form = ProductInvoiceForm(request.POST, instance=pinvoice_data)
        if invoice_form.is_valid() and pinvoice_form.is_valid():
            inv = invoice_form.save(commit=False)
            print(inv)
            pinv = pinvoice_form.save(commit=False)
            inv.user = request.user
            inv.active = 1
            inv = inv.save()
            inv_obj = Invoices.objects.get(id=inv.id)
            if inv_obj:
                print(inv_obj.customer.name)
                pinv.invoices = inv_obj.id
                pinv.active = 1
                pinv.save()
                return HttpResponseRedirect("/mysite/invoice/")
            else:
                print("Invoice is not getting through database")
    template_name = "myadmin/invoice/add-invoice.html"
    return render(request, template_name, {'invoice_data': invoice_data,
                                           'pinvoice_data': pinvoice_data,
                                           'products': Products.objects.all(),
                                           'customers': Customers.objects.all(),
                                           'status_choices': STATUS_CHOIES,
                                           'note_condition':note_condition},)


def invoice_detail(request, pk):
    invoice_obj = Invoices.objects.get(user=request.user, id=pk)
    pinvoice_obj = ProductInvoice.objects.filter(invoices=invoice_obj)
    template_name = "myadmin/invoice/invoice_detail.html"
    return render(request,template_name, {'invoice_obj': invoice_obj, "pinvoice_obj": pinvoice_obj})


def invoice_delete(request, pk):
    obj = Invoices.objects.get(user=request.user, id=pk)
    obj.active = False
    obj.save()
    return redirect('/mysite/invoice/')



def product_change(request, slug=None):
    obj = None
    if slug:
        obj = Products.objects.get(user=request.user, slug=slug)
    if request.method == "POST":
        form = ProductForm(request.POST, instance=obj)
        if form.is_valid():
            new_upload = form.save(commit=False)
            new_upload.user = request.user
            new_upload.active = 1
            new_upload.severity = 1
            new_upload.status = 1
            new_upload.save()
            return HttpResponseRedirect('/mysite/product/')

    else:
        return render(request, "myadmin/invoice/product/product.html", {'obj': obj,
                                                        'product_type': PRODUCT_TYPE_CHOICES,
                                                        'unit': UNIT_CHOICES})


def list_page(request):
    obj = Products.objects.filter(user=request.user, active=True)
    template_name = "myadmin/invoice/product/list.html"
    return render(request, template_name, {'obj': obj})


def detail_page(request, slug):
    obj = Products.objects.get(user=request.user, slug=slug)
    template_name = "myadmin/invoice/product/detail.html"
    return render(request,template_name, {'obj': obj})


def delete_page(request, pk):
    obj = Products.objects.get(id=pk)
    obj.active = False
    obj.save()
    return redirect('/mysite/product/')


def customer_change(request, slug=None):
    customerdetails = None
    if slug:
        customerdetails = Customers.objects.get(user=request.user, slug=slug)

    if request.method == "POST":
        form = CustomerForm(request.POST, instance=customerdetails)
        if form.is_valid():
            new_case = form.save(commit=False)
            new_case.user = request.user
            new_case.status = 1
            new_case.severity = 1
            new_case.active = 1
            new_case.save()
            return HttpResponseRedirect('/mysite/customer/')

    template_name = "myadmin/invoice/customer/customer.html"
    return render(request, template_name, {'customerdetails': customerdetails,
                                           'customer_type': CUSTOMER_CHOICE,
                                           'currency': CURRENCY_CHOICES,
                                           'payment_terms': PAYMENT_TERMS_CHOICE,
                                           'country': Country.objects.all(),
                                           'portal_language': LANGUAGE_CHOICES})


def customer_list(request):
    obj = Customers.objects.filter(user=request.user, active=True)
    template_name = "myadmin/invoice/customer/list.html"
    return render(request, template_name, {'obj': obj})


def customer_detail(request, pk):
    obj = Customers.objects.get(user=request.user, id=pk)
    template_name = "myadmin/invoice/customer/detail.html"
    return render(request, template_name, {'obj': obj})


def customer_delete(request, pk):
    obj = Customers.objects.get(user=request.user, id=pk)
    obj.active = False
    obj.save()
    return redirect('/mysite/customer/')

