from django import forms
from .models import *

class TaxRateForm(forms.ModelForm):
    class Meta:
        model = TaxRate
        exclude = ('user', 'status', 'severity', 'slug')

class GstSettingForm(forms.ModelForm):
    class Meta:
        model = GstSetting
        exclude = ('user', 'status', 'severity', 'slug')

class CurrencyForm(forms.ModelForm):
    class Meta:
        model = Currency
        exclude = ('user', 'status', 'severity', 'slug')  




