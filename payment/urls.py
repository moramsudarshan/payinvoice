from django.conf.urls import url
from .views import payment_history, purchase_credit, payment, my_plan, features

urlpatterns = [
    url(r'payment/history/$', payment_history, name='payment_history'),
    url(r'payment/purchase-credit/$', purchase_credit, name='purchase_credit'),
    url(r'payment/myplan/$', my_plan, name='my_plan'),
    url(r'payment/purchase-credit/(?P<plan_id>\d+)/$', purchase_credit, name='purchase_credit_id'),
    url(r'payment/$', payment, name="payment"),
    url(r'payment/plan/features/$', features, name="features"),
]
