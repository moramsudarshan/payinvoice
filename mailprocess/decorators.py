from functools import wraps
from django.utils.decorators import available_attrs
import json
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import resolve_url


# def login_required_ajax(test_func, login_url=None):
def login_required_ajax(view_func):
    @wraps(view_func, assigned=available_attrs(view_func))
    def _wrapped_view(request, *args, **kwargs):
        if request.user.is_authenticated:
            return view_func(request, *args, **kwargs)
        else:
            if request.is_ajax():  # or request.method == "POST":
                return HttpResponse(json.dumps(5), content_type="application/json")
            else:
                # path = request.build_absolute_uri()
                path = request.get_full_path()
                resolved_login_url = resolve_url("/accounts/signin/")
                from django.contrib.auth.views import redirect_to_login
                return redirect_to_login(path, resolved_login_url)

    return _wrapped_view


def check_credit_subscribers(view_func):
    @wraps(view_func, assigned=available_attrs(view_func))
    def _wrapped_view(request, *args, **kwargs):
        if request.user.userdetail.myplan.plan_type == 2 and request.user.userdetail.remaining_emails < 0:
            if request.is_ajax():  # or request.method == "POST":
                return HttpResponse(json.dumps(6), content_type="application/json")
            else:
                return HttpResponseRedirect("/mysite/")
        else:
            return view_func(request, *args, **kwargs)

    return _wrapped_view


'''def check_report_permissions(required_permissions=None):
    from campaign_manager.models import Campaign

    def _method_wrapper(view_func):
        def _arguments_wrapper(request, *args, **kwargs):
            campaign_id = kwargs.get('campaign_id')
            if not campaign_id:
                return HttpResponseRedirect("/mysite/permission/denied/")
            else:
                try:
                    campaign = Campaign.objects.get(id=int(campaign_id))
                    if campaign.report_access < int(required_permissions):
                        return HttpResponseRedirect(
                            "/mysite/permission/denied/?campaign_id=%s&type=permission" % (campaign_id))
                    else:
                        return view_func(request, *args, **kwargs)
                except:
                    return HttpResponseRedirect("/mysite/permission/denied/?campaign_id=%s&type=except" % (campaign_id))

        return _arguments_wrapper

    return _method_wrapper'''
