from django.contrib import admin
from django.utils.html import format_html
from .models import *


class UserProfileAdmin(admin.ModelAdmin):
    list_display = (
    'user_email', 'country', 'mobile', 'skypeid', 'username', 'account_type', 'key', 'is_api_enabled', 'total_subscribers',
    'remaining_subscribers',  'valid_date', 'supportplan_valid_date', 'last_login', 'date_joined', 'login_as_user')
    list_filter = ('user__date_joined', 'user__last_login', 'account_type', )
    search_fields = ('user__email', 'user__username')

    def country(self, obj):
        if obj.country_detail:
            return obj.country_detail.name + "-idd-%s, ccd-+%s" % (obj.country_detail.idd, obj.country_detail.ccd)
        else:
            return ''

    def login_as_user(self, obj):
        return format_html("<a href='/accounts/signin/{}/'>login</a>", obj.user.username)

    login_as_user.allow_tags = True

    def first_name(self, obj):
        return obj.user.first_name

    def last_name(self, obj):
        return obj.user.last_name

    def last_login(self, obj):
        return obj.user.last_login

    def date_joined(self, obj):
        return obj.user.date_joined

    def user_email(self, obj):
        return obj.user.email

    def username(self, obj):
        return obj.user.username




class UserUsageAdmin(admin.ModelAdmin):
    list_display = ('username', 'product', 'quantity', 'price', 'active', 'date')
    list_filter = ('product__name', 'userusage__user',)

    def username(self, obj):
        return obj.userusage.user.username

    def reseller_usage(self, obj):
        return obj.reseller_price()


class BroadcastAdmin(admin.ModelAdmin):
    list_display = ('message', 'active', 'added_date', 'modified_date')


admin.site.register(UserUsage, UserUsageAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Broadcast, BroadcastAdmin)
