from django.contrib import admin
from .models import *

class TaxAdmin(admin.ModelAdmin):
    list_display=('tax_name', 'rate')    

class GstAdmin(admin.ModelAdmin):
    list_display=('gstin','gstdate','compositionscheme')

class CurrenciAdmin(admin.ModelAdmin):
    list_display=('currency_code', 'currency_symbol', 'decimal_places', 'choose_format')

class TemplateAdmin(admin.ModelAdmin):
    list_display = ('name','template_type','preview_image','path','thumbnail')

class UserTemplateAdmin(admin.ModelAdmin):
    list_display = ('template','customer')

class NotesAndCoditionsAdmin(admin.ModelAdmin):
    list_display = ('customer_notes', 'terms_and_conditions')

admin.site.register(TaxRate, TaxAdmin)
admin.site.register(GstSetting, GstAdmin)
admin.site.register(Currency, CurrenciAdmin)
admin.site.register(Template,TemplateAdmin)
admin.site.register(UserTemplate, UserTemplateAdmin)
admin.site.register(NotesAndConditions, NotesAndCoditionsAdmin)
