from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .forms import SupportCasesForm
from .models import *
from django.http import HttpResponse, HttpResponseRedirect


@login_required
def support(request):
    template_name = "myadmin/support/support.html"
    supportcase = SupportCases.objects.filter(active=True, user=request.user)
    return render(request, template_name, {'supportcase': supportcase})


@login_required
def support_create(request):
    template_name = "myadmin/support/support_create.html"
    if request.method == "POST":
        form = SupportCasesForm(request.POST)
        if form.is_valid():
            new_case = form.save(commit=False)
            new_case.user = request.user
            new_case.status = 1
            new_case.severity = 1
            new_case.active = 1
            new_case.save()
            return HttpResponseRedirect("/mysite/support/")
    supportcategory = SupportPlanCategory.objects.filter(active=True)
    return render(request, template_name, {'supportcategory': supportcategory})


@login_required
def support_detail(request, case_id):
    template_name = "myadmin/support/support_detail.html"
    supportcase = None
    try:
        supportcase = SupportCases.objects.get(id=case_id, user=request.user)
    except:
        return HttpResponseRedirect("/mysite/support/")
    return render(request, template_name, {'supportcase': supportcase})


@login_required
def support_reopen(request, case_id):
    template_name = "myadmin/support/support_reopen.html"
    if request.method == "POST":
        comment = request.POST.get("comment")
        supportcasedetail = SupportCasesDetail(supportcases_id=case_id, user=request.user, comment=comment)
        supportcasedetail.save()
        supportcasedetail.supportcases.status = 5
        supportcasedetail.supportcases.save()
        return HttpResponseRedirect("/mysite/support/detail/%s/" % (case_id))
    return render(request, template_name, {})
