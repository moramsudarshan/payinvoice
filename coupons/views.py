from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.db.models import Q
from django.conf import settings
from .models import *


def offers(request):
    coupons = Coupon.objects.filter(user__isnull=True, active=True, expires_on__gte=datetime.now())
    template_name = 'static/offers.html'
    discount_6_mon = settings.DURATION_DISCOUNT.get("2")
    discount_12_mon = settings.DURATION_DISCOUNT.get("3")
    return render(request, template_name,
                  {'coupons': coupons, 'discount_6_mon': discount_6_mon, 'discount_12_mon': discount_12_mon})


@login_required
def myoffers(request):
    coupons = Coupon.objects.filter(active=True, expires_on__gte=datetime.now()).filter(
        Q(user__isnull=True) | Q(user=request.user))
    template_name = 'myadmin/coupons/myoffers.html'
    discount_6_mon = settings.DURATION_DISCOUNT.get("2")
    discount_12_mon = settings.DURATION_DISCOUNT.get("3")
    return render(request, template_name,
                  {'coupons': coupons, 'discount_6_mon': discount_6_mon, 'discount_12_mon': discount_12_mon})
