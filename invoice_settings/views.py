from django.shortcuts import render, redirect
from django.shortcuts import render, get_object_or_404
from .forms import *
from .models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.views.generic.edit import UpdateView



 #   currency add,edit, delete starts here #



def currency_change(request, slug=None):
    currencydetails=None
    if slug:
        currencydetails=Currency.objects.get(user=request.user, id= slug)
    if request.method == "POST":
        form = CurrencyForm(request.POST, instance=currencydetails)
        if form.is_valid():
            new_case = form.save(commit=False)
            new_case.user = request.user
            new_case.status = 1
            new_case.severity = 1
            new_case.active = 1
            new_case.save()
            return HttpResponseRedirect("/mysite/invoicesetting/currency/")
    template_name = "myadmin/invoice_setting/currency/add-currency.html"
    return render(request, template_name, {'cur':currencydetails, 'currencycode' : CURRENCY_CODE_CHOICES,'currencysymbol':CURRENCY_SYMBOL_CHOICES, 'decimalplaces' : DECIMAL_PLACES_CHOICES, 'formating':FORMAT_CHOICES  }) 



#currency_listing
def currency(request):
    template_name = "myadmin/invoice_setting/currency/currencylist.html"
    currency = Currency.objects.filter(user=request.user, active= True)
    return render(request, template_name, {'currency':currency})

#currency_delete
def currency_delete(request, pk):
    gstdetails = Currency.objects.get(id=pk)
    gstdetails.active = False
    gstdetails.save()
    return redirect('/mysite/invoicesetting/currency/')       


#   currency add,edit, delete ends here #






  #   tax add,edit, delete starts here #

#listing_of_taxes
def tax(request):
    template_name = "myadmin/invoice_setting/tax/taxlist.html"
    taxes = TaxRate.objects.filter(user=request.user, active= True)
    return render(request, template_name, {'taxes':taxes})


# create and update_tax
def tax_change(request, slug=None):
    taxdetails = None
    if slug:
        taxdetails = TaxRate.objects.get(user=request.user, id=slug)  

    if request.method == "POST":
        form = TaxRateForm(request.POST, instance=taxdetails)
        if form.is_valid():
            new_case = form.save(commit=False)
            new_case.user = request.user
            new_case.status = 1
            new_case.severity = 1
            new_case.active = 1
            new_case.save()
            return HttpResponseRedirect("/mysite/invoicesetting/tax/")
    else:
        template_name = "myadmin/invoice_setting/tax/add-tax.html"
        return render(request, template_name, {'tax' : taxdetails} )

#delate_tax
def tax_delete(request, pk):
    taxdetails = TaxRate.objects.get(id=pk)
    taxdetails.active = False
    taxdetails.save()
    return redirect('/mysite/invoicesetting/tax/') 


 #   tax add,edit, delete ends here #



#   gst add,edit,delate starts here #
       

def gst(request):
    template_name = "myadmin/invoice_setting/gst/gstlist.html"
    gstdetails = GstSetting.objects.filter(user=request.user, active= True)
    return render(request, template_name, {'gstdetails':gstdetails})


def gst_change(request, slug=None):
    gstdetails = None
    if slug:
     gstdetails = GstSetting.objects.get(user=request.user, id=slug)    

    if request.method == "POST":
        form = GstSettingForm(request.POST, instance=gstdetails)
        if form.is_valid():
            new_case = form.save(commit=False)
            new_case.user = request.user
            new_case.status = 1
            new_case.severity = 1
            new_case.active = 1
            new_case.save()
            return HttpResponseRedirect("/mysite/invoicesetting/gst/")
    else:
        template_name = "myadmin/invoice_setting/gst/add-gst.html"
        return render(request, template_name, {'gst' : gstdetails, 'compositescheme':COMPOSITE_SCHEME_CHOICES} ) 



def gst_delete(request, pk):
    gstdetails = GstSetting.objects.get(id=pk)
    gstdetails.active = False
    gstdetails.save()
    return redirect('/mysite/invoicesetting/gst/')    

#   gst add,edit, delete ends here #


def invoice_template(request, slug=None):
    template = Template.objects.filter(active=True)
    # import pdb; pdb.set_trace()
    template_type_var=0
    if slug=='template':
        template=template.filter(template_type=0)
       
    else:
        template=template.filter(template_type=1)
        template_type_var=1

    try:
        usertemplate=UserTemplate.objects.get(customer=request.user, template__template_type=template_type_var)
        
    except:
        usertemplate=None
    template_name ='myadmin/invoice_setting/invoice_template.html'
    return render(request,template_name, {'templates':template, 'usertemplate':usertemplate},)


def invoice_template_add(request, slug=None):
    template_id = request.GET.get("template_id")
    # import pdb; pdb.set_trace()
    template_type_var=0
    if slug !='template':
        template_type_var=1
    if template_id:
        template= Template.objects.get(id=template_id)
        try:
            usertemplate=UserTemplate.objects.get(customer=request.user, template__template_type=template_type_var)
            usertemplate.template=template
            usertemplate.save()
        except:
            usertemplate=UserTemplate()
            usertemplate.customer=request.user
            usertemplate.template=template
            usertemplate.save()
    return HttpResponseRedirect("/mysite/invoicesetting/"+slug+"/")
   