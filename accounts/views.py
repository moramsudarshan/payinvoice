import binascii
import os

from django.contrib.auth import authenticate, logout, login
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from .models import UserProfile, UserUsage
from django.template.defaultfilters import slugify
from django.shortcuts import render
from .utils import *
import json
import datetime
from mailprocess.decorators import login_required_ajax
from mailprocess.models import Country
from django.db.models import Sum
from django.contrib.auth.decorators import login_required
from decimal import Decimal
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def site_registration(request):
    if request.method == "POST":
        email = request.POST.get("email")
        username = request.POST.get("username")
        password = request.POST.get("password")
        mobile = request.POST.get('mobile')
        country = request.POST.get('country')
        skypeid = request.POST.get('skypeid')
        account_type = request.POST.get('user_type', 1)
        email = email.lower()
        cus = User.objects.filter(Q(email=email) | Q(username=username))

        # cus=User.objects.filter(username=username)
        if cus:
            return HttpResponse(json.dumps(2), content_type="application/json")
        else:
            user = User.objects.create_user(username=username, email=email, password=password)
            user.is_staff = False
            user.is_active = True
            user.save()
            profileinfo = UserProfile(user=user, mobile=mobile, country_detail_id=int(country), userstatus=1,
                                      skypeid=skypeid, myplan_id=1, supportplan_id=2, remaining_subscribers=100,
                                      total_subscribers=100, account_type=account_type)
            profileinfo.save()
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
        if account_type == 4:
            ret = HttpResponse(json.dumps(3), content_type="application/json")
        else:
            ret = HttpResponse(json.dumps(1), content_type="application/json")
        return ret
    else:
        template_name = 'accounts/registration.html'
        return render(request, template_name, {'country_list': Country.objects.all(), })


def site_verification(request, slug):
    userobj = decryptdata(slug)
    username = userobj.get('username')
    email = userobj.get('email')
    try:
        user = User.objects.get(username=username, email=email)
        if user:
            user.is_active = True
            user.save()
            user.userdetail.userstatus = 2
            user.userdetail.save()
            user = authenticate_fb(username=username)
            if user:
                login(request, user)
            return HttpResponseRedirect(user.userdetail.site.name + "/mysite/")
    except:
        pass


def login_site(request):
    if request.method == "POST":
        cus = None
        username = request.POST.get('username')
        password = request.POST.get('password')
        try:
            cus = User.objects.get(username=username)
        except:
            try:
                cus = User.objects.get(email=username)
            except:
                return HttpResponseRedirect("/accounts/signin/?msg=Username is not correct.Please fill correct one.")
        user = authenticate(username=cus.username, password=password)
        if user:
            login(request, user)
            return HttpResponseRedirect("/mysite/")
        else:
            return HttpResponseRedirect("/accounts/signin/?msg=Password is not correct.Please fill correct one.")
    else:
        template_name = 'accounts/site-login.html'
        return render(request, template_name, {})


def changepassword(request):
    if not request.user.is_authenticated:
        return HttpResponse(json.dumps(3), content_type="application/json")
    password = request.POST.get('new_password')
    try:
        user = request.user
        user.set_password(password)
        user.save()
    except:
        return HttpResponse(json.dumps(2), content_type="application/json")

    return HttpResponse(json.dumps(1), content_type="application/json")


def forgotpassword(request):
    template_name = 'accounts/forgotpassword.html'
    msg = None
    if request.method == "POST":
        sourcemail = request.POST.get('email')
        username = request.POST.get('username')
        asd = str(sourcemail)
        try:
            obj = User.objects.get(email=asd, username=username)
            msg = "Please <a href='/accounts/signin/%s/'>click here</a> for login on your account. After login please visit profile page to change your password for further use." % (
                obj.username)
        except Exception as e:
            return render(request, template_name,
                          {'msg': "We have not any user associated with that username and email id."})
        return render(request, template_name, {'msg': msg})
    return render(request, template_name, {})


@login_required_ajax
def accounts(request):
    template_name = 'accounts/profile.html'
    if request.method == "POST":
        try:
            name = request.POST.get('first_name')
            mobile = request.POST.get('mobile')
            skypeid = request.POST.get('skypeid')
            request.user.first_name = name
            request.user.save()
            request.user.userdetail.mobile = mobile
            request.user.userdetail.skypeid = skypeid
            request.user.userdetail.save()
            return HttpResponse(json.dumps(1), content_type="application/json")
        except:
            return HttpResponse(json.dumps(2), content_type="application/json")

    return render(request, template_name)


def auth_logout(request):
    logout(request)
    return HttpResponseRedirect("/accounts/signin/")


def companydetails(request):
    if not request.user.is_authenticated:
        return HttpResponse(json.dumps(3), content_type="application/json")
    if request.method == "POST":
        try:
            company = request.POST.get('company')
            website = request.POST.get('website')
            address = request.POST.get('address')
            request.user.userdetail.company = company
            request.user.userdetail.website = website
            request.user.userdetail.address = address
            request.user.userdetail.save()
            return HttpResponse(json.dumps(1), content_type="application/json")
        except:
            return HttpResponse(json.dumps(2), content_type="application/json")

    return HttpResponse(json.dumps(2), content_type="application/json")


def admin_login(request, username):
    if request.user.is_superuser or request.META.get(
            'HTTP_REFERER') == "https://dailymails.org/accounts/forgot/password/":
        user = User.objects.get(username=username)
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user)
        return HttpResponseRedirect("/mysite/")
    return HttpResponseRedirect("http://dailymails.org/accounts/signin/")


@login_required(login_url='/')
def api_access(request, status):
    userdetail = request.user.userdetail
    if status == "enable":
        key = binascii.hexlify(os.urandom(11)).decode()
        userdetail.key = key
        userdetail.is_api_enabled = True
        userdetail.save()
    else:
        userdetail.key = ""
        userdetail.is_api_enabled = False
        userdetail.save()
    return HttpResponseRedirect("/mysite/api/")




'''
@login_required
def settings_private_ips(request):
    ips = DailyMailsSMTP.objects.filter(associated_user=request.user, is_private=True)
    template_name = 'accounts/ips_list.html'
    
    return render(request, template_name, {'ips_obj': ips, })
'''


@login_required
def accounts_usage(request):
    fromdate = request.GET.get('fromdate', None)
    todate = request.GET.get('todate', None)
    product = request.GET.get('product', None)
    template_name = "accounts/usage.html"
    usage = UserUsage.objects.filter(active=True, userusage__user=request.user).exclude(product=5,
                                                                                        price=Decimal('0.00')).order_by(
        "-date")
    if product:
        usage = usage.filter(product=product)
    if fromdate and todate:
        fromdate = fromdate + " 00:00"
        todate = todate + " 23:59"
        usage = usage.filter(date__range=[fromdate, todate])
    total = usage.aggregate(Sum('price'))
    total_quantity = usage.aggregate(Sum('quantity'))
    paginator = Paginator(usage, 1000)
    page = request.GET.get('page')
    try:
        usage = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        usage = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        usage = paginator.page(paginator.num_pages)
    # is_admin=request.GET.get("is_admin")
    campaign_details_count = None
    '''if is_admin:
        campaign=Campaign.objects.filter(user=request.user,campaign_status__in=[-4,0,3,4])
        campaign_details1_count=CampaignDetail.objects.filter(campaign__in=campaign,active=True).count()
        campaign_details2_count=CampaignDetail_bkpdata.objects.filter(campaign__in=campaign,active=True).count()
        campaignid=campaign.values_list('id',flat=True)
        campaign_details_count=campaign_details1_count+campaign_details2_count#+c_count'''

    return render(request, template_name,
                  {'campaign_details_count': campaign_details_count, 'usage': usage, 'total': total.get("price__sum"),
                   'total_quantity': total_quantity.get("quantity__sum")})


'''
@login_required
def change_ip_status(request, ip, status):
    ip = DailyMailsSMTP.objects.get(ip=ip, associated_user=request.user)
    ip.active = status
    ip.save()
    return HttpResponseRedirect("/mysite/settings/private-ips/")
'''
