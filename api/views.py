from django.http import Http404
from invoice.models import Products, Customers, Invoices, ProductInvoice
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .serializers import ProductsSerializers, CustomersSerializers, \
    InvoicesSerializers, DownloadInvoicesSerializers, \
    NestedProductInvoiceSerializers, NestedInvoicesSerializers
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics

class ProductView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        # obj = Products.objects.all()
        obj = Products.objects.filter(user=request.user, active=True)
        serializer = ProductsSerializers(obj, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = ProductsSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductDetailView(APIView):
    permission_classes = [IsAuthenticated]

    def get_object(self, request, pk):
        try:
            return Products.objects.get(pk=pk, user=request.user)
        except Products.DoesNotExist:
            raise Http404

    def get(self, request, pk, ):
        try:
            obj = self.get_object(request,pk)
            serializer = ProductsSerializers(obj)
            return Response(serializer.data)
        except Products.DoesNotExist as e:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, pk):
        try:
            obj = Products.objects.get(pk=pk, user=request.user)
            # obj.active = False
            # obj.save()
            obj.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Products.DoesNotExist as e:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):

        obj = Products.objects.get(pk=pk, user=request.user)

        serializer = ProductsSerializers(instance=obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=200)

        return Response(serializer.errors, status=400)


class CustomerList(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        obj = Customers.objects.filter(user=request.user, active=True)
        serializer = CustomersSerializers(obj, many=True)
        return Response(serializer.data)

    def post(self, request, ):
        serializer = CustomersSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CustomerDetail(APIView):
    permission_classes = [IsAuthenticated]

    def get_object(self, request, pk):
        try:
            return Customers.objects.get(pk=pk, user=request.user)
        except Products.DoesNotExist as e:
            return Response({"error": "Given Product not found."}, status=404)

    def get(self, request, pk, ):
        try:
            obj = self.get_object(request,pk)
            serializer = CustomersSerializers(obj)
            return Response(serializer.data)
        except Customers.DoesNotExist as e:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, pk):
        try:
            obj = Customers.objects.get(pk=pk, user=request.user)
            # obj.active = False
            # obj.save()
            obj.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Products.DoesNotExist as e:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def put(self, request, pk):

        obj = Customers.objects.get(pk=pk, user=request.user)

        serializer = CustomersSerializers(instance=obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=200)

        return Response(serializer.errors, status=400)


class NestedInvoiceView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]

    queryset = Invoices.objects.all()
    serializer_class = NestedInvoicesSerializers

class NestedInvoiceDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]

    queryset = Invoices.objects.all()
    serializer_class = NestedInvoicesSerializers


# class InvoiceView(APIView):
#     permission_classes = [IsAuthenticated]
#
#     def get(self, request):
#         obj = Invoices.objects.filter(user=request.user, active=True)
#         serializer = InvoicesSerializers(obj, many=True)
#         return Response(serializer.data)
#
#     def post(self, request, ):
#         serializer = InvoicesSerializers(data=request.data)
#         if serializer.is_valid():
#             serializer.save(user=request.user)
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#         # Invoices_serializer.is_valid(raise_exception=True)
#         # serializer.create(validated_data=request.data)
#         # Invoices_serializer = InvoicesSerializers(instance=Invoices)
#         # if Invoices_serializer.is_valid():
#         #     Invoices = Invoices_serializer.save()
#         #     Invoices_serializer = InvoicesSerializers(instance=Invoices)
#         #     return Response(Invoices_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#
# class InvoiceDetailview(APIView):
#     permission_classes = [IsAuthenticated]
#
#     def get_object(self, pk):
#         try:
#             return Invoices.objects.get(pk=pk, user=request.user)
#         except Invoices.DoesNotExist as e:
#             raise Http404
#
#     def get(self, request, pk, ):
#
#         obj = self.get_object(pk)
#         serializer = InvoicesSerializers(obj)
#         return Response(serializer.data)
#
#     def delete(self, request, pk):
#         try:
#             obj = Invoices.objects.get(pk=pk, user=request.user)
#             obj.active = False
#             obj.save()
#             return Response(status=status.HTTP_204_NO_CONTENT)
#         except Invoices.DoesNotExist as e:
#             return Response(status=status.HTTP_404_NOT_FOUND)


class Pdfview(APIView):
    permission_classes = [IsAuthenticated]

    def get_object(self, pk):
        try:
            return Invoices.objects.get(pk=pk, user=request.user)

        except Invoices.DoesNotExist as e:
            return Response({"error": "Given object not found."}, status=404)

    def get(self, request, pk, ):

        obj = self.get_object(pk)
        serializer = DownloadInvoicesSerializers(obj)

        return Response(serializer.data)


# class pinvoice(APIView):
#     permission_classes = [IsAuthenticated]
#
#     def get(self, request):
#         obj = ProductInvoice.objects.filter(user=request.user)
#         serializer = ProductInvoiceSerializers1(obj, many=True)
#         return Response(serializer.data)
#
#     def post(self, request, ):
#         serializer = ProductInvoiceSerializers1(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
