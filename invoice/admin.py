from django.contrib import admin
from .models import *


class ProductAdmin(admin.ModelAdmin):
    list_display = ('product_type', 'name', 'unit', 'selling_price', 'sku',  'description')


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'company_name', 'customer_type', 'email', 'phone', 'website',
                    'billing_country', 'billing_state', 'billing_state', 'billing_city', 'billing_zipcode',
                    'billing_address', 'shipping_country', 'shipping_state', 'shipping_city', 'shipping_zipcode',
                    'shipping_address', 'currency', 'payment_terms', 'enable_portal', 'portal_language')


class ProductInvoiceAdmin(admin.TabularInline):
    model = ProductInvoice


class InvoiceAdmin(admin.ModelAdmin):
    inlines = [ProductInvoiceAdmin]


admin.site.register(Invoices, InvoiceAdmin)
admin.site.register(Customers, CustomerAdmin)
admin.site.register(Products, ProductAdmin)
