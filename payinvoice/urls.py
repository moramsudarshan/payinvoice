from django.urls import include, path
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^', include('mailprocess.urls')),
    url(r'^', include('accounts.urls')),
    url(r'^', include('myadmin.urls')),
    url(r'^', include('coupons.urls')),
    url(r'^mysite/', include('support.urls')),
    url(r'^mysite/', include('payment.urls')),
    url(r'^mysite/', include('invoice.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^mysite/', include('invoice_settings.urls')),
    url(r'^mysite/api/v1/', include('api.urls')),


]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
