from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^support/$', support, name='support'),
    url(r'^support/create/$', support_create, name='support-create'),
    url(r'^support/reopen/(?P<case_id>\d+)/$', support_reopen, name='support_reopen'),
    url(r'^support/detail/(?P<case_id>\d+)/$', support_detail, name='support-detail'),
]
