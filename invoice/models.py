from django.db import models
from django.db.models.base import ModelBase
from django.utils.text import slugify
from mailprocess.models import *

PAYMENT_TERMS_CHOICE = (
    (0, 'NET15'), (1, 'NET30'), (2, 'NET45'), (3, 'NET60'), (4, 'Due end of the month'), (5, 'Due end of next month'))
LANGUAGE_CHOICES = ((0, 'English'),)
CURRENCY_CHOICES = (
    (0, 'AUD- Australian Dollar'), (1, 'CAD- Canadian Dollar'), (2, 'EUR- Euro'), (3, 'INR- Indian Rupee'),
    (4, 'USD- United States Dollar'), (5, 'SAR- Saudi Riyal'))
UNIT_CHOICES = ((0, 'm'), (1, 'cm'), (2, 'gm'), (3, 'kg'), (4, 'pc'), (5, 'dozen'), (6, 'units'))
TERMS_CHOICES = (
    (0, 'NET15'), (1, 'NET30'), (2, 'NET45'), (3, 'NET60'), (4, 'Due end of the month'), (5, 'Due end of next month'),
    (6, 'Due on Receipt'), (7, 'Custom'))
CHALLAN_TYPE_CHOICES = ((0, 'supply of liquid gas'), (1, 'job work'), (2, 'supply on approval'), (3, 'other'))
PRODUCT_TYPE_CHOICES = ((0, 'Goods'), (1, 'Services'))
CUSTOMER_CHOICE = ((1, "Business"), (2, "Individual"))
DISCOUNT_UNIT = ((0, '%'), (1, 'Amount'))
STATUS_CHOIES = ( (1,"draft"), (3,"paid"), (5,"sent"))

class Customers(ModelBase):
    customer_type = models.IntegerField(choices=CUSTOMER_CHOICE, default=0)
    first_name = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=100, null=True, blank=True)
    company_name = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField()
    phone = models.CharField(max_length=20, null=True, blank=True)
    website = models.CharField(max_length=200, null=True, blank=True)
    billing_country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True, blank=True)
    billing_state = models.CharField(max_length=200, null=True, blank=True)
    billing_city = models.CharField(max_length=200, null=True, blank=True)
    billing_zipcode = models.CharField(max_length=12, null=True, blank=True)
    billing_address = models.CharField(max_length=200, null=True, blank=True)
    currency = models.IntegerField(choices=CURRENCY_CHOICES, default=0)
    payment_terms = models.IntegerField(choices=PAYMENT_TERMS_CHOICE, default=0)
    enable_portal = models.BooleanField(default=False)
    portal_language = models.IntegerField(choices=LANGUAGE_CHOICES, default=0)
    shipping_country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True, blank=True,
                                         related_name='shipping')
    shipping_state = models.CharField(max_length=150, null=True, blank=True)
    shipping_city = models.CharField(max_length=150, null=True, blank=True)
    shipping_zipcode = models.CharField(max_length=14, null=True, blank=True)
    shipping_address = models.CharField(max_length=150, null=True, blank=True)
    slug = models.SlugField(max_length=250, unique=True)

    def save(self, *args, **kwargs):
        if not self.id:
            super(Customers, self).save(*args, **kwargs)
        self.slug = slugify(self.first_name) + "-" + str(self.id)
        super(Customers, self).save(*args, **kwargs)


class Products(ModelBase):
    product_type = models.IntegerField(choices=PRODUCT_TYPE_CHOICES, default=0)
    name = models.CharField(max_length=256)
    unit = models.IntegerField(choices=UNIT_CHOICES, default=0)
    selling_price = models.DecimalField(max_digits=9, decimal_places=2)
    sku = models.CharField(max_length=100, null=True, blank=True)
    product_image = models.ImageField(upload_to='upload/products/', null=True, blank=True)
    description = models.CharField(max_length=200, null=True, blank=True)
    slug = models.SlugField(max_length=250, unique=True)

    def save(self, *args, **kwargs):
        if not self.id:
            super(Products, self).save(*args, **kwargs)

        self.slug = slugify(self.name) + "-" + str(self.user.id) + "-" + str(self.id)

        super(Products, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.name)


class Invoices(ModelBase):
    customer = models.ForeignKey(Customers, related_name='invoice_customer', on_delete=models.CASCADE, )
    invoice_number = models.CharField(max_length=100)
    order_number = models.CharField(max_length=100, null=True, blank=True)
    invoice_date = models.DateField()
    terms = models.IntegerField(choices=TERMS_CHOICES, default=0)
    due_date = models.DateField()
    subject = models.CharField(max_length=200, null=True, blank=True)
    subtotal = models.DecimalField(max_digits=9, decimal_places=2)
    total = models.DecimalField(max_digits=9, decimal_places=2)
    pdf_code = models.TextField(max_length=250, null=True, blank=True)
    status = models.IntegerField(choices=STATUS_CHOIES, default=1)

    # customer_notes = models.CharField(max_length=200, null=True, blank=True)
    # terms_and_conditions = models.TextField()  #moved it to invoice settings


class ProductInvoice(models.Model):
    products = models.ForeignKey(Products, related_name='product', on_delete=models.CASCADE, )
    invoices = models.ForeignKey(Invoices, related_name='productinvoices', on_delete=models.CASCADE,)
    quantity = models.DecimalField(max_digits=9, decimal_places=2)
    rate = models.DecimalField(max_digits=9, decimal_places=2)
    discount = models.DecimalField(max_digits=9, decimal_places=2)
    discount_unit = models.IntegerField(choices=DISCOUNT_UNIT, default=1)
    amount = models.DecimalField(max_digits=9, decimal_places=2)
    slug = models.SlugField(max_length=255, unique=True)
    active = models.BooleanField(default=True)


    # def save(self, *args, **kwargs):
    #     if not self.id:
    #         super(ProductInvoice, self).save(*args, **kwargs)
    #
    #     self.slug = slugify(self.amount) + "-" + str(self.invoices.user.id) + "-" + str(self.id)
    #
    #     super(ProductInvoice, self).save(*args, **kwargs)


'''class DeliveryChallan(ModelBase):
    customer_name=models.ForeignKey(Customers,on_delete=models.CASCADE)
    delivery_challan_number=models.CharField(max_length=20)
    reference_number=models.CharField(max_length=20, null=True, blank=True)
    delivery_challan_date=models.DateField()
    challan_type=models.IntegerField(choices=CHALLAN_TYPE_CHOICES,default=0)
    product_details=models.ForeignKey(Products,on_delete=models.CASCADE)
    quantity=models.CharField(max_length=200, null=True, blank=True)
    rate=models.DecimalField(max_digits=9,decimal_places=2)
    discount=models.DecimalField(max_digits=9,decimal_places=2)
    amount=models.DecimalField(max_digits=9,decimal_places=2)
    subtotal=models.DecimalField(max_digits=9,decimal_places=2)
    total=models.DecimalField(max_digits=9,decimal_places=2)
    customer_notes=models.CharField(max_length=200, null=True, blank=True)
    terms_and_conditions=models.TextField()'''
