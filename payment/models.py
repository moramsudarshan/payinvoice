from django.db import models
from django.contrib.auth.models import User

PAYMENT_CHOICE = (('1', "Pending Payment"), ('2', "Verified"))
SOURCE_CHOICE = (('1', 'Prepaid'), ('2', "POST PAID"))
REPORT_ACCESS = ((0, "No Reporting"), (1, "Basic Reporting"), (2, "Advanced Reporting"))
PLAN_TYPE = ((1, "Subscriber-based Plan"), (4, "Premium Support Plan"),)
DURATION_CHOICE = (("1", "1 Month"), ("2", "6 Months"), ("3", "12 Months"))


class PaymentPlan(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    price = models.PositiveIntegerField(default=10)
    count = models.PositiveIntegerField(default=5000)
    active = models.BooleanField(default=True)
    plan_type = models.IntegerField(choices=PLAN_TYPE, default=1)
    report_access = models.IntegerField(choices=REPORT_ACCESS, default=1)

    def __str__(self):
        return str(self.name)


class PaymentTrack(models.Model):
    payment_id = models.CharField(max_length=256, null=True, blank=True)
    status = models.CharField(max_length=1, choices=PAYMENT_CHOICE, default='1')
    source = models.CharField(max_length=1, choices=SOURCE_CHOICE, default='1')
    duration = models.CharField(max_length=1, choices=DURATION_CHOICE, default='1')
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    amount_received = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    rate = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="payment_user")
    plan = models.ForeignKey(PaymentPlan, on_delete=models.CASCADE)
    added_date_time = models.DateTimeField(auto_now_add=True)
    payment_date_time = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        if self.payment_id:
            return str(self.payment_id)
        return str(self.id)
