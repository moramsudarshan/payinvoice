from django.db import models
from django.contrib.auth.models import User
from payment.models import PaymentPlan, PaymentTrack
from mailprocess.models import ModelBase


# SKU_COUPON = 'A/P/C-A/U-30-080519'


class Coupon(ModelBase):
    COUPON_TYPE = (
        ('1', 'All'),
        ('2', 'Plan'),
        ('3', 'Category')
    )
    COUPON_CONDITION = (
        (1, 'On Purchase of Plan'),
        (2, 'On First Payment')
    )

    coupon_code = models.CharField(max_length=10, unique=True, null=False, blank=False)
    discount = models.IntegerField()
    expires_on = models.DateField(blank=False, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    coupon_type = models.CharField(choices=COUPON_TYPE, max_length=1)
    plan_or_category = models.ForeignKey(PaymentPlan, on_delete=models.CASCADE, null=True, blank=True)
    condition = models.IntegerField(choices=COUPON_CONDITION)


    def __str__(self):
        return str(self.coupon_code)


class CouponUsage(ModelBase):
    coupon = models.ForeignKey(Coupon, on_delete=models.CASCADE)
    payment = models.ForeignKey(PaymentTrack, on_delete=models.CASCADE)
