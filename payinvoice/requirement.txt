django==2.2
mysqlclient==1.4.6
requests==2.23.0
djangorestframework==3.11.0
Pillow==2.2.1
